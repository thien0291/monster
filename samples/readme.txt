This folder contains sample application using Monster framework. To run
these application, it requires installed panda3d and monster-framework.

To run sample:
- Navigate to sample folder. Ex: HelloMonster
- Run below command:
python app.py

Note: if you use PyDev, you can run app.py like run normal python file.