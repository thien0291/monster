#from Monster.core.objects.baseobject import Store
#from Monster.core.modules.managers.MapManager import MapManager, TotalMapManager, CacheMapManager
#from Monster.core.objects.map import Map
#from Monster.utility import getModule
import __builtin__
#from Monster.core.modules.managers.manager import GameEntityManager
from direct.directnotify.DirectNotify import DirectNotify
from Monster.core.modules.managers.scenemanager import SceneManager



class Monster():
    
    notify = DirectNotify().newCategory("Monster")
    
    def __init__(self):
#        self.store = Store()
#        self.totalMgr = TotalMapManager()
#        self.cacheMgr = CacheMapManager()
#        self.mapMgr = self.cacheMgr             
        self.updateBuiltin()
        
#    def editorMode(self, active):
#        if active == True:
#            self.mapMgr = self.totalMgr
#        else:
#            self.cacheMgr = self.totalMgr
#        self.updateBuiltin()
            
    def updateBuiltin(self):
        Monster.notify.debug("update builtin")
        # Global variables
#        __builtin__.store = self.store
#        __builtin__.mapMgr = self.mapMgr
#        __builtin__.screen = Map
#        __builtin__.getModule = getModule
#        __builtin__.entityMgr = GameEntityManager
        __builtin__.scenes = SceneManager()
        
__builtin__.game = Monster()

import extend

moduleNames = ["Monster.extend.objects.directgui"]
map(__import__, moduleNames) 


# Global Variables

