#import from python
import json
import sys, os
import copy
#import from Panda3d
from panda3d.core import *
from direct.particles.Particles import Particles
from direct.particles.ParticleEffect import ParticleEffect
from direct.particles.ForceGroup import ForceGroup
from direct.gui.DirectGui import *
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.DirectObject import DirectObject
from panda3d.bullet import *
from direct.actor.Actor import Actor
# from Monster.core.modules.managers.MapManager import nodeRoot
from Monster.extend.objects.sky import *
from Monster.extend.objects.terrain import *
from Monster.extend.objects.water import *
from Monster.extend.objects.grass import *
from Monster.core.objects.camera import *
from Monster.extend.objects.light import *
from Monster.extend.objects.fog import *
from Monster.extend.objects.model import *
from Monster.utility.guihelper import getGeom
from Monster.core.objects.baseBuilder import BaseBuilder
from Monster.extend.objects.shadow import SingleShadow, ShadowFactory, ShadowBase
from Monster.core.objects.input import Panda3DInput
from Monster.extend.objects.particle import ParticleBase
from Monster.core.modules.managers.gui import GuiManager



    
    
class ParticleBuilder(BaseBuilder):
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Particles'][Data['objectID']]
        particle = copy.deepcopy(prototype)  
        effect = self.createParticle(particle.describeDict)
        # effect.reparentTo(particle.nodePath)
        particle.setEffect(effect)
        if Data.has_key("Pos"):     
            posParam = caster.stringListToListFloat(Data['Pos'].split(','))
            particle.nodePath.setPos(*posParam)
        if Data.has_key("Scale"):
            scaleParam = caster.stringListToListFloat(Data['Scale'].split(','))
            particle.nodePath.setScale(*scaleParam)  
        particle.nodePath.reparentTo(nodeRoot)      
        particle.startEffect(nodeRoot)
        prototypeData = prototype.describeDict
        if Data.has_key('PhysicBody'):
            if prototypeData.has_key('PhysicBody'):
                prototypeData['PhysicBody'].update(Data['PhysicBody'])
            else:
                prototypeData['PhysicBody'] = Data['PhysicBody']
        # Bullet physical checking
        self.processBulletData(prototypeData, particle, nodeRoot, world)
        self.processScripts(Data, particle)
        self.processAttributes(Data, particle)
        return particle

    def createParticle(self, Data):
        particleEffect = ParticleEffect()
        particleEffect.loadConfig(Data['Path'])
        return particleEffect
        
    def buildType(self, Data):
        base.enableParticles()        
        particleNP = NodePath(Data['Path'])                
        # particleNP.hide(BitMask32.bit(4))
        particle = ParticleBase(nodeName=Data['Name'], nodePath=particleNP, describeDict=Data)  
        centerNP = particle.center      
        centerNP.setDepthWrite(False)
        centerNP.setShaderOff(1)
        centerNP.setLightOff(1)
        centerNP.setBin('fixed', 40)
        self.processTags(Data, particle)  
        return particle
    
# Device input builder    
class InputBuilder():    
    def buildInput(self):
        pass
    
class Panda3DInputBuilder():
    def buildInput(self, key, data):
        tags = data['Tags'].split(',')
        for tag in tags:
            tags[tags.index(tag)] = tag.strip()  # Make it well format
        pandaInput = Panda3DInput(key, tags)
        return pandaInput
    
# 2D builder

class GuiBuilder():
    def buildGui(self):
        pass
    
class DirectGuiBuilder():
    def buildGui(self, guiMgr, var_name, data):         
        # GUI type        
        # print "build gui", data
        gui_type = data['type']
        data.pop('type')        
        # Process data   
        self.processEvents(data)
        self.processScripts(data, guiMgr)
        self.processGuiScripts(data, var_name, guiMgr)
        self.processParent(data, guiMgr)
        self.processGeom(data, 'geom')
        self.processGeom(data, 'thumb_geom')
        self.processGeom(data, 'boxGeom')
        self.processEval(data)
        self.processFont(data)
        trans = self.processTrans(data)
        # Parse data
        self.parseData(data)
        
        # print "gui data", data
        gui = getattr(sys.modules[__name__], gui_type)(**data)   
        gui.setName(var_name)
        if trans:
            gui.setTransparency(TransparencyAttrib.MAlpha) 
            texture = gui.findTexture('*')            
            if texture:
                texture.setWrapU(Texture.WMClamp)
                texture.setWrapV(Texture.WMClamp)
        return gui
    
    def processEvents(self, data):
        self.processEvent('command', data)
        self.processEvent('focusInCommand', data)
        self.processEvent('focusOutCommand', data)
        self.processEvent('itemMakeFunction', data)
        
    def processGuiScripts(self, data, var_name, guiMgr):
        if data.has_key('gui_scripts'):
            scripts_data = data['gui_scripts']
            init_script = self.processScript('init', scripts_data)
            run_script = self.processScript('run', scripts_data)
            data.pop('gui_scripts')
            if init_script:
                guiMgr.addGuiScript(var_name, init_script)
            if run_script:
                guiMgr.addGuiScript(var_name, run_script, 'run')
            
        
    def processScripts(self, data, guiMgr):
        if data.has_key('scripts'):
            scripts_data = data['scripts']
            init_script = self.processScript('init', scripts_data)
            run_script = self.processScript('run', scripts_data)
            data.pop('scripts')
            if init_script:
                guiMgr.addScript(init_script)
            if run_script:
                guiMgr.addScript(run_script, 'run')
            
    
    def processScript(self, tagname, data):
        if data.has_key(tagname):
            filepath = data[tagname][0]
            funcName = data[tagname][1]
            modules = self.getModule(filepath)
            for module in modules:
                func = getattr(module, funcName)
                return func                
                  
                
    def processParent(self, data, guiMgr):
        if data.has_key('parent'):
            parent = data['parent']                
            if parent == "render2d" or parent == "aspect2d":
                data['parent'] = eval(data['parent'])
            else:
                parentGui = guiMgr.getGui(parent)   
                if parentGui:
                    data['parent'] = parentGui     
                    
    def processFont(self, data):
        options = ['font', 'text_font']
        for key in options:
            if data.has_key(key):
                font_file = data[key]
                font = loader.loadFont(font_file)
                data[key] = font
    

    def getModule(self, filepath):
        scriptFile = filepath
        PROJECT_FOLDER = os.getcwd()
        scriptFilePath = os.path.join(PROJECT_FOLDER, scriptFile)
        name = os.path.basename(scriptFilePath)
        name, ext = os.path.splitext(name)
        SCRIPT_FOLDER = os.path.dirname(scriptFilePath)
        if sys.path.count(SCRIPT_FOLDER) == 0:
            sys.path.append(SCRIPT_FOLDER)
        scriptFile = [name]
        # print "script file", scriptFile
        modules = map(__import__, scriptFile)
        return modules

    def processEvent(self, tagname, data):
        if data.has_key(tagname):  
            if isinstance(data[tagname], list):  
                funcName = data[tagname][1]     
                filepath = data[tagname][0]       
                modules = self.getModule(filepath)
                for module in modules:
                    data[tagname] = getattr(module, funcName)          
                    # print "event", data['command']  
    
    def processInherit(self, data):
        if data.has_key('inherit'):
            inherit = data['inherit']
            data.pop('inherit')
            if isinstance(inherit, str) or isinstance(inherit, unicode):
                json_data = open(inherit)
                inherit_data = json.load(json_data)
            else:
                inherit_data = inherit
            data_load = self.processInherit(inherit_data)
            if data_load.has_key('childs'):
                for key, child in data_load['childs'].items():
                    inherit_data_child = self.processInherit(child)
                    inherit_data_child.update(data_load['childs'][key])
                    data_load['childs'][key] = inherit_data_child
            inherit_data.update(data_load)
            return inherit_data
        return {}
    
    def processTrans(self, data):
        if data.has_key('trans'):
            trans = eval(data['trans'])
            data.pop('trans')
            return trans
            
    
    def processEval(self, data):
        evalList = ['align', 'text_align']
        for evalItem in evalList:
            if data.has_key(evalItem):
                data[evalItem] = eval(data[evalItem])
        
      
    
    def processGeom(self, data, geomKey):
        if data.has_key(geomKey):
            geom_data = data[geomKey]
            geom = getGeom(geom_data['MapPath'], *geom_data['ImageNames'])            
            data[geomKey] = geom
            return geom
        
    
    def parseData(self, data):
        for key, value in data.items(): 
            if isinstance(value, list):
                data[key] = tuple(value)
        
