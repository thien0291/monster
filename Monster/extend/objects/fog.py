from Monster.core.objects.baseobject import GameObject
import sys
import copy
from Monster.core.objects.baseBuilder import BaseBuilder
from Monster.utility import caster
from panda3d.core import Fog


class FogBuilder(BaseBuilder):
        
    def addFogShaderInputToNode(self, node, fog):
        fogMode = fog.getMode()        
        if fogMode is Fog.MLinear:
            node.setShaderInput('fog_mode', 0)                        
        elif fogMode is Fog.MExponential:
            node.setShaderInput('fog_mode', 1)            
        elif fogMode is Fog.MExponential:
            node.setShaderInput('fog_mode', 2)
            
            
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):          
        fog = copy.deepcopy(gameObjectList['Fogs'][Data['objectID']])
        if Data.has_key('Affects'):
            affectList = Data['Affects'].split(',')
            for modelID in affectList.items():
                node = models[modelID].nodePath
                node.setFog(fog.nodePath)
                self.addFogShaderInputToNode(node, fog)
        else:            
            nodeRoot.setFog(fog.nodePath)
            self.addFogShaderInputToNode(nodeRoot, fog)
        
        
        return fog
    
    def buildType(self, Data):
        tempFog = Fog(Data['Name'])
        if Data['Type'].lower() == "linearfog":
            tempFog.setMode(Fog.MLinear)
        elif Data['Type'].lower() == "exponential":
            tempFog.setMode(Fog.MExponential)
        else:
            tempFog.setMode(Fog.MExponentialSquared)
        
        color = caster.stringListToListFloat(Data['Color'].split(','))
        tempFog.setColor(*color)
        if Data.has_key('LinearRange') and len(Data['LinearRange']) > 1:
            linearData = caster.stringListToListFloat(Data['LinearRange'].split(','))
            tempFog.setLinearRange(*linearData)
        if Data.has_key('LinearFallback') and len(Data['LinearFallback']) > 2:
            linearFallBackData = caster.stringListToListFloat(Data['LinearFallback'].split(','))
            tempFog.setLinearFallback(*linearFallBackData)
        if Data.has_key('ExpDensity'):
            if Data['ExpDensity'] != None:                
                tempFog.setExpDensity(caster.stringToFloat(Data['ExpDensity'], 0.005))
        
        base.setBackgroundColor(*color)
        fog = FogFactory.createFog(Data['Type'], nodePath=tempFog, nodeName=Data['Name'], describeDict=Data)        
        render.setShaderInput('fog_mode', -1)  # fix shader error because missing input           
        self.processTags(Data, fog)  
        return fog



class FogFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createFog(fogType, *args, **kwargs):
        fog = getattr(sys.modules[__name__], 'FogBase')(*args, **kwargs)
        return fog
    
class FogBase(GameObject):
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)        
        
    def doNothing(self, *args, **kwargs):
        pass
    
    def getMode(self):
        return self.nodePath.getMode()

