from panda3d.core import * 
from Monster.core.objects.baseobject import GameObject
from Monster.extend.objects.shadow import SingleShadow, PSSMShadow
from Monster.core.objects.baseBuilder import BaseBuilder


class TerrainBuilder(BaseBuilder):
        
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):             
        prototype = gameObjectList['Terrains'][Data['objectID']]
        terrain = self.buildTerrrain(prototype.describeDict, Data)
        prototypeData = prototype.describeDict
        # Bullet physical checking
        self.processBulletData(prototypeData, terrain, nodeRoot, world)    
        
        # AdditionData
        
        if Data.has_key('Pos'):            
            posParam = caster.stringListToListFloat(Data['Pos'].split(','))
            terrain.nodePath.setPos(*posParam)
        
        if Data.has_key('Hpr'):            
            posParam = caster.stringListToListFloat(Data['Hpr'].split(','))
            terrain.nodePath.setHpr(*posParam)
        
        
        terrain.nodePath.reparentTo(nodeRoot)
        self.processTags(Data, terrain) 
        self.processScripts(Data, terrain)    
        return terrain        
    
    def buildTypeMonoTexture(self, Data):
        # #print "TRACE: TerrainBuilder --> builtType()"
        terrain = GeoMipTerrain(Data['Name'])
        terrain.setHeightfield(Data['HeightFieldPath'])
        terrain.setBlockSize(caster.stringToInt(Data['BlockSize'], 0))
        terrain.setNear(caster.stringToFloat(Data['Near'], 0))
        terrain.setFar(caster.stringToFloat(Data['Far'], 0))
        terrain.setFocalPoint(base.camera)
        posParam = caster.stringListToListFloat(Data['Pos'].split(','))
        
        terrainNode = terrain.getRoot()        
        terrainNode.setSz(caster.stringToFloat(Data['Setsz'], 0))
        terrainNode.setPos(*posParam)
#        terrain.setMinLevel(caster.stringToInt(Data['MinLevel'], 0))
        terrain.setAutoFlatten(GeoMipTerrain.AFMOff)
        
        # Set Texture
        terrainNode.clearTexture()
        scaleParam = caster.stringListToListFloat(Data['tScale'].split(','))
        terrainNode.setShaderInput('tscale', Vec4(*scaleParam))
        
        ts = TextureStage('ts')
        tex = base.loader.loadTexture(Data['TexturePath'])
        terrainNode.setTexture(ts, tex)
        terrain.generate()
        
        terrainBase = TerrainBase(nodeName=Data['Name'], nodePath=terrainNode, describeDict=Data)
        # terrainBase.nodePath.reparentTo(nodeRoot)
        return terrainBase
    
    def buildTerrrain(self, Data, instanceData):                
        terrain = myGeoMipTerrain(Data['Name'])
        terrain.setHeightfield(Data['HeightFieldPath'])
        
        loaded = False
        if Data.has_key('CacheFile'):
            if os.path.exists(Data['CacheFile']):
                root = loader.loadModel(Data['CacheFile'])
                loaded = True
        if not loaded:
            # Set terrain properties
            if Data.has_key('BlockSize'):
                terrain.setBlockSize(caster.stringToInt(Data['BlockSize'], 32))        
            terrain.setFocalPoint(base.camera)
            if Data.has_key('Near'):
                terrain.setNear(caster.stringToFloat(Data['Near'], 0.01))
            if Data.has_key('Far'):
                terrain.setFar(caster.stringToFloat(Data['Far'], 1000))
            if Data.has_key('MinLevel'):
                terrain.setMinLevel(caster.stringToFloat(Data['MinLevel'], 0.01))            
            # print "generate terrain", terrain        
            # Generate it.
            terrain.generate()                        
            
            if Data.has_key("CacheFile"):
                # print "write cache file"
                terrain.getRoot().writeBamFile(Data['CacheFile'])
        
            # Store the root NodePath for convenience
            root = terrain.getRoot()        
        
        if Data.has_key('Setsz'):
            root.setSz(caster.stringToFloat(Data['Setsz'], 0))  # z (up) scale
        if Data.has_key('Setsz'):
            root.setShaderInput('tscale', Vec4(*caster.stringListToListFloat(Data['tscale'].split(','))))  # texture scaling
        if Data.has_key('Setsz'):
            root.setShaderInput('scale', 1, 1, caster.stringToFloat(Data['Setsz'], 2), 1)
        
        

        # texture
        terrain.setMultiTexture(Data, root)      

        self.processMaterialInfo(Data, root)
        
        terrainBase = TerrainBase(nodeName=Data['Name'], nodePath=root, describeDict=Data, terrain=terrain)
                 
        return terrainBase        
    
    def buildType(self, Data):
        terrainBase = TerrainBase(nodeName=Data['Name'], nodePath=None, describeDict=Data, terrain=None)
        self.processScripts(Data, terrainBase)
        self.processTags(Data, terrainBase)    
        return terrainBase       
        
        
class TerrainBase(GameObject):
    def __init__(self, *args, **kwargs):
        self.terrain = kwargs['terrain']
        print "Init terrain", self.terrain
        GameObject.__init__(self, *args, **kwargs)
        # Default settings
#        self.updateAmbientLight(Vec4(0.1, 0.1, 0.1, 1.0))
#        self.updateDirectLight(Vec4(0.7, 0.7, 0.7, 1.0))
#        self.updateLightVector(Vec3(1, 0, 0))
        
    def clone(self):
        clone = GameObject.clone(self)
        print "INIT CLONE" + str(clone)
        clone.setNodePath(clone.terrain.generate())    
        return clone
        
    def start(self):
#        self.myTask = taskMgr.add(self.update, "TerrainUpdate" + self.nodeName)
        pass
    
    def stop(self):
#        taskMgr.remove("TerrainUpdate" + self.nodeName)
        pass
    
    def update(self, task):
        print self.terrain
        self.terrain.update()
        return task.cont
        
    def activeShadow(self, shadow=None):        
        if isinstance(shadow, SingleShadow):
            self.realNP.setTag('Normal', 'Shadow')            
            print "shadow terrain"
        elif isinstance(shadow, PSSMShadow):
            print "pssm shadow terrain"
            self.realNP.setTag('Normal', 'PSSMShadow')     
        else:
            print "shadow terrain normal"   
            self.realNP.setTag('Normal', 'NoShadow')      
                    

from Monster.utility import caster
import random
import  sys, os

SPEED = 0.5

# Figure out what directory this program is in.
MYDIR = os.path.abspath(sys.path[0])
MYDIR = Filename.fromOsSpecific(MYDIR).getFullpath()
    
class myGeoMipTerrain(GeoMipTerrain):
    def __init__(self, name):
        GeoMipTerrain.__init__(self, name)        
        
    def update(self):
#        print "Self ", self
#        print self is GeoMipTerrain
#        print "GEOMIPTERRAIN", GeoMipTerrain
        GeoMipTerrain.update(self)
        
    def setMonoTexture(self):
        root = self.getRoot()
        ts = TextureStage('ts')
        tex = loader.loadTexture('textures/land01_tx_512.png')
        root.setTexture(ts, tex)
        
    def _setup_camera(self, shaPath, tagStateName):        
        sa = ShaderAttrib.make()
        sa = sa.setShader(loader.loadShader(shaPath))   
        
        cam = base.cam.node()
        cam.setLens(base.camLens)
        cam.setTagStateKey('Normal')
        cam.setTagState(tagStateName, RenderState.make(sa)) 
        
        
                
    def setMultiTexture(self, Data, root):
        
        
        for idx in xrange(1, 5):
            key = 'tex{0}Path'.format(idx)
            if Data.has_key(key):
                tex = loader.loadTexture(Data[key])            
                tex.setMinfilter(Texture.FTNearestMipmapLinear)
                tex.setMagfilter(Texture.FTLinear)
                ts = TextureStage('tex{0}'.format(idx))  # stage 0
                root.setTexture(ts, tex)    
            
        # Alpha map
        for idx in xrange(1, 5):
            key = 'alp{0}Path'.format(idx)
            if Data.has_key(key):
                tex = loader.loadTexture(Data[key])            
                tex.setMinfilter(Texture.FTNearestMipmapLinear)
                tex.setMagfilter(Texture.FTLinear)
                ts = TextureStage('alp{0}'.format(idx))  # stage 0
                root.setTexture(ts, tex)        
        # Detail map
        if Data.has_key('detailPath'):        
            tex = loader.loadTexture(Data['detailPath'])            
            tex.setMinfilter(Texture.FTNearestMipmapLinear)
            tex.setMagfilter(Texture.FTLinear)
            ts = TextureStage('detail'.format(idx))  # stage 0
            root.setTexture(ts, tex)  
         
        # Set shaders apply on terrains
        root.setTag("Normal", "NoShadow")
#        root.setTag("Clipped", "True") # temp , remove when fix water.py
        
        # Setup shaders
        shadowPath = 'Shader/shadowterrain.sha'
        pssmShadowPath = 'Shader/pssmshadowterrain.sha'
        if Data.has_key('ShadowShaderPath'):
            shadowPath = Data['ShadowShaderPath']
        if Data.has_key('PSSMShadowShaderPath'):
            shadowPath = Data['PSSMShadowShaderPath']    
        self._setup_camera(Data['shaderPath'], 'NoShadow')
        self._setup_camera(shadowPath, 'Shadow')
        self._setup_camera(pssmShadowPath, 'PSSMShadow')
