'''This contains all model entity like: human, car, building, tree, flower, etc...
These models is loaded from .egg or .bam file.
These models 'now' only references to:
    - nodepath.
    - parent (model).
    - nodename (use for searching in scene graph).
    
HOW TO USE:
    - First, you call createModel static method of ModelFactory class.
    - Finally, you retreive the result by variable.
'''
#import from sys
import sys
import json
#import from Panda3D
from direct.actor.Actor import Actor
from panda3d.bullet import BulletCharacterControllerNode
from panda3d.core import Vec3, NodePath
#import from Monster Framework
from Monster.core.objects.baseobject import GameObject
from Monster.core.objects.baseBuilder import BaseBuilder
from Monster.utility import caster

    
class GameModelBuilder(BaseBuilder):
    def __init__(self):
        pass
    

    def createObject(self, Data, gameObjectList, nodeRoot, world, prototype):
        model = prototype.clone()        
        model.setName(Data['Name'])
        if Data.has_key("Pos") and len(Data['Pos']) > 2 :
            posParam = Data['Pos']
            if not isinstance(posParam, list):
                posParam = caster.stringListToListFloat(Data['Pos'].split(','))            
            model.nodePath.setPos(*posParam)
        if Data.has_key("Hpr") and len(Data['Hpr']) > 2:
            hprParam = Data['Hpr']
            if not isinstance(hprParam, list):
                hprParam = caster.stringListToListFloat(Data['Hpr'].split(','))  
            model.nodePath.setHpr(*hprParam)
        self.processTags(Data, model)
        self.processAttributes(Data, model)
        prototypeData = prototype.describeDict
        if Data.has_key('PhysicBody'):
            if prototypeData.has_key('PhysicBody'):
                prototypeData['PhysicBody'].update(Data['PhysicBody'])
            else:
                prototypeData['PhysicBody'] = Data['PhysicBody']
        # Bullet physical checking
        self.processBulletData(prototypeData, model, nodeRoot, world)
        if Data.has_key('Parent'):
            if Data['Parent'] == "":
                model.nodePath.reparentTo(nodeRoot)
            else:
                model.nodePath.reparentTo(gameObjectList['GameModels'][Data['Parent']])
        else:
            model.nodePath.reparentTo(nodeRoot)
        self.processScripts(Data, model)
        # Update describe dict
        model.describeDict.update(Data)
        # print "BUILD OBJECT DONE"
        return model

    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):        
        # print "DATA@@@@@@@ ", Data
        # print "GameObjectList", gameObjectList
        prototype = gameObjectList['GameModels'][Data['objectID']] 
        if Data.has_key('TransformFile'):
            # print "transform file", Data['TransformFile']
            transform = json.load(open(Data['TransformFile']))
            positions = transform['positions']
            name = Data['Name']
            model = GameObject(name)
            idx = 0      
            for idx, position in enumerate(positions):
                if transform.has_key('hprs'):
                    Data['Hpr'] = transform['hprs'][idx]
                Data['Pos'] = position
                Data['Name'] = name + str(idx)
                child = self.createObject(Data, gameObjectList, model.nodePath, world, prototype)                
                # print "build instance"
                idx += 1      
            model.reparentTo(nodeRoot)                  
        else:  
            model = self.createObject(Data, gameObjectList, nodeRoot, world, prototype)
        return model
    
    def asynchronize(self, *nodePaths):
        pass

    def buildType(self, data):
        # #print "TRACE: GameModelBuilder --> BuilderGameModel"
        
        modelParts = {}
        anims = {}
        isActor = False
        nodeName = data['Name']  # It can override when add to Map_
        if data.has_key('Parts'):
            for part in data['Parts'].values():
                # Add Model Path to modelParts Dict
                name = part['Model']['Name']
                path = part['Model']['Path']
                modelParts[name] = path
                
                # Add animation to anims Dict.
                # Each Part maybe have many Animation.
                # So each Part have a Animation Dict
                
                anim = {}                        
                if part.has_key('Anims'):
                    for item in part['Anims'].values():
                        isActor = True
                        animPath = item['Path']
                        animName = item['Name']
                        anim[animName] = animPath
                    anims[name] = anim
                
            # Load for Panda View
            # -- Load panda model OR Actor            
            if(isActor == True):            
                nodePath = Actor(modelParts, anims)
                nodePath.setName(nodeName)
            else:
                nodePath = base.loader.loadModel(modelParts.values()[0])
                nodePath.setName(nodeName)
        else:
            nodePath = NodePath(nodeName)    
        # -- Set basic Attribute
        # -- -- Set Default Position             
        if data.has_key("Pos") and len(data['Pos']) > 2:  
            posParam = caster.stringListToListFloat(data['Pos'].split(','))
            nodePath.setPos(*posParam)
            
        # -- -- Set Default Hpr
        if data.has_key("Hpr") and len(data['Hpr']) > 2:
            hprParam = caster.stringListToListFloat(data['Hpr'].split(','))
            nodePath.setHpr(*hprParam)
        
        # -- -- Set Default Color
        if data.has_key("Color") and len(data['Color']) > 3:
            colorParam = data['Color'].split(',')
            colorParamList = caster.stringListToListFloat(colorParam)
            nodePath.setColor(*colorParamList)
        
        # -- -- Set Default Scale
        if data.has_key("Scale") and len(data['Scale']) > 2:
            scaleParam = caster.stringListToListFloat(data['Scale'].split(','))
            nodePath.setScale(*scaleParam)
            
        self.processMaterialInfo(data, nodePath)
        
        # -- Set TwoSide Default
        if data.has_key("TwoSided"):
            nodePath.setTwoSided(caster.stringToBool(data['TwoSided'], 0))
        
        if data.has_key("Bin") and len(data['Bin']) > 1:
            binParam = data['Bin'].split(',')
            nodePath.setBin(binParam[0], caster.stringToInt(binParam, 0))
        
        model = ModelFactory.createModel(data['Type'],
                                         nodePath=nodePath,
                                         nodeName=nodeName,
                                         describeDict=data) 
        self.processTags(data, model)       
        self.processScripts(data, model)     
        self.processAttributes(data, model)
            
        return model

class ModelFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createModel(modelType, *args, **kwargs):
        model = getattr(sys.modules[__name__], modelType)(*args, **kwargs)
        return model
    
class GameModel(GameObject):    
    def clone(self):
        clone = GameObject.clone(self)
        if isinstance(self.nodePath, Actor):
            clone.nodePath = Actor(other=self.nodePath)
        return clone    
    
# Empty nodePath
class Component(GameObject):
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        
# Organism in the worl
class Organism(GameModel):
    '''All about animal, human, plant, insects, ... live in the world.'''
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        self.crouching = False
        self.direction = Vec3(0, 0, 0)
        self.speed = 1
        self.omega = 0
        self.jumpHeight = 5
        self.jumpSpeed = 8
        
    # Edit in use (these lines of code below is only an example)
    def move(self):  
        if isinstance(self.physicNode.node(), BulletCharacterControllerNode):
            node = self.physicNode.node()
            node.setAngularMovement(self.omega)
            node.setLinearMovement(self.direction * self.speed, True)
        
    # Edit in use (these lines of code below is only an example)    
    def jump(self):
        if isinstance(self.physicNode.node(), BulletCharacterControllerNode):
            self.player.setMaxJumpHeight(self.jumpHeight)
            self.player.setJumpSpeed(self.jumpSpeed)
            self.player.doJump()
      
    
    def doCrouch(self):
        if isinstance(self.physicNode.node(), BulletCharacterControllerNode):
            self.crouching = not self.crouching
            sz = self.crouching and 0.6 or 1.0 
            node = self.physicNode.node()
            node.getShape().setLocalScale(Vec3(1, 1, sz))     
            self.physicNod.setScale(Vec3(1, 1, sz) * 0.3048)
            self.physicNod.setPos(0, 0, -1 * sz)
        
class Human(Organism):
    pass

class Monster(Organism):
    pass

class Insect(Organism):
    pass

class Plant(Organism):
    pass

class Tree(Organism):
    pass

class Rock(Organism):
    pass

class Animal(Organism):
    pass

class Architecture(GameModel):
    pass

class Building(Architecture):
    '''Building class.'''
    pass

class Road(Architecture):
    pass

class Vehicle(GameModel):      
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        self.vehicle = None
        # Steering info
        self.steersIdx = []
        self.forcesIdx = []
        self.steering = 0.0           
        self.steeringClampMax = 45.0     
        self.steeringIncrementMax = 5 
        self.turnState = 'stand'
        
        # Engine info        
        self.force = 0
        self.brake = 0     
        
        # Wheels
        self.wheels = []
    
    def destroy(self):    
        for wheel in self.wheels:
            wheel.removeNode()
        
    def setMaxForce(self, value):
        self.tuning = self.vehicle.getTuning()
        self.tuning.setMaxSuspensionForce(value)
                
    def go(self, force=100, brake=2):
        self.force = force
        self.brake = brake
        
    def back(self, force=100, brake=2):
        self.brake = brake
        self.force = -force
        
    def slide(self):
        self.brake = abs(self.force * 1.0 / 100)
        self.force = 0
        
    def turnRight(self):
        self.mark = -1
        self.turnState = 'turn'
        
    def turnLeft(self):        
        self.mark = 1
        self.turnState = 'turn'
        
    def notTurn(self):
        self.turnState = 'stand'
        
        
        
            
    def updateVehicle(self):   
        dt = globalClock.getDt()        
        if self.turnState == 'turn':          
            self.steeringIncrement = self.steeringIncrementMax / max (abs(self.vehicle.getCurrentSpeedKmHour()) / 3, 1) 
            self.steeringClamp = self.steeringClampMax / max (abs(self.vehicle.getCurrentSpeedKmHour() / 90), 1)
            self.steering += self.mark * dt * self.steeringIncrement * 30
            self.steering = self.mark * min(abs(self.steering), self.steeringClamp)
        else:
            dt *= 5
            self.steering = self.steering * (1 - dt) + 0 * dt
        
        # Apply engine and brake to rear wheels
        for idx in self.forcesIdx:
            self.vehicle.applyEngineForce(self.force, idx)
            self.vehicle.setBrake(self.brake, idx)
        # Apply steering to front wheels
        for idx in self.steersIdx:
            self.vehicle.setSteeringValue(self.steering, idx)
      

class HouseHold(GameModel):
    pass

class Item(GameModel):
    pass

class Car(Vehicle):
    '''Car class.'''
    pass
