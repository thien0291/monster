'''
Created on May 1, 2014

@author: thinhtran
'''


from direct.showbase.DirectObject import DirectObject
from direct.directnotify.DirectNotify import DirectNotify
from direct.gui.DirectGui import DirectFrame
from Monster.core.objects.gamenode import GameNode, GameNodeBuilder


class DirectGUILayer(GameNode):
    """
    Layer contains panda3d DirectGUI items such as: DirectButton, DirectFrame, ...
    """
    
    notify = DirectNotify().newCategory("Monster.core.modules.managers.guimanager.DirectGUILayer")
        
    def __init__(self, name):
        GameNode.__init__(self, name)
        self.items = []
        
    ########### List magic methods ################
    def __len__(self):
        return len(self.items)

    def __getitem__(self, key):
        # if key is of invalid type or value, the list values will raise the error
        return self.items[key]

    def __setitem__(self, key, value):
        self.items[key] = value

    def __delitem__(self, key):
        del self.items[key]

    def __iter__(self):
        return iter(self.items)
    ######### End list magic methods ###############
    
    def append(self, item):
        if isinstance(item, DirectFrame):            
            self.items.append(item)
            item.reparentTo(self.nodePath)
        else:
            GUILayer.notify.error("Added item is not DirectFrame object")
            
    def remove(self, item):
        if item in self.items:
            self.items.remove(item)
            
    def pop(self, index):
        if not index:
            index = len(self.items) - 1
            
        self.items.pop(index)
        
        

class DirectGUIBuilder(GameNodeBuilder):
    
    notify = DirectNotify().newCategory("Monster.extend.objects.directgui.GameNodeBuilder")
    
    def _refresh(self, node, optionDefs):
        for optionDef in optionDefs:
            optionName, optionValue, handler = optionDef
            node[optionName] = optionValue
            if handler:
                handler()
    
    def make(self, xmlRoot):
        DirectGUIBuilder.notify.debug("DirectGUIBuilder.make")
        if xmlRoot.get("class") is not None:
            cls = getattr(sys.modules[__name__], xmlRoot.get("class"))
            node = cls()
        else:
            node = DirectFrame() 
        
        node.setName(xmlRoot.tag)          
        optionDefs = self.createOptionDefs(xmlRoot, nodePath)
        self._refresh(node, optionDefs)
        
        node.resetFrameSize()
        node.updateFrameStyle()
        
        return node
    
    def createOptionDefs(self, xmlRoot, parentNode): 
        optionDefs = GameNodeBuilder.createOptionDefs(self, elementTree, parentNode)
         
        # write your stuff 
        # parsing lxml ElementTree
        for child in elementTree.getchildren():
            if child.get("type") == "node":
                builder = self
                node = builder.make(child)
                node.reparentTo(parentNode)
            else:
                # create optionDefs for property
                optionName = str(child.tag).lower()
                optionValue = child.text
                datatype = child.get("datatype", "")
                if datatype == "list float":
                    paramString = child.text.split(",")
                    optionValue = caster.stringListToListFloat(paramString, 0)
                optionDef = [optionName, optionValue, None]
                optionDefs.append(optionDef)
        # end stuff
              
        return optionDefs
