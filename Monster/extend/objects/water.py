from Monster.core.objects.baseobject import GameObject
from panda3d.core import CardMaker, NodePath, TransparencyAttrib, Vec4, PlaneNode, Plane, CullFaceAttrib
from panda3d.core import Vec3, Point3, RenderState, ShaderAttrib, Texture, TextureStage
from Monster.core.objects.baseBuilder import BaseBuilder

class WaterBuilder(BaseBuilder):
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Waters'][Data['objectID']]
        water = prototype.clone()
        water.nodePath.reparentTo(nodeRoot)
        if Data.has_key('Pos'):            
            posParam = caster.stringListToListFloat(Data['Pos'].split(','))
            water.nodePath.setPos(*posParam)
            
        if Data.has_key('Hpr'):            
            posParam = caster.stringListToListFloat(Data['Hpr'].split(','))
            water.nodePath.setHpr(*posParam)
                
        return water
    
    def buildType(self, Data):
        # _water_level with z is height of water
        _water_level = Vec4(0.0, 0.0, caster.stringToFloat(Data['Height'], 0), 1.0)
        # _water_node (x1, y1, x2, y2, z) Left-Top Point and Right-Bottom Point , Z - height of water
        size = caster.stringListToListFloat(Data['Size'].split(","))
        if Data.has_key('Pos'):
            pos = caster.stringListToListFloat(Data['Pos'].split(","))
        else:
            pos = [0, 0]   
        # #print size[0], size[1]
        halfWidth = size[0] / 2
        halfHeight = size[1] / 2
        water = WaterNode(pos[0] - halfWidth, pos[1] - halfHeight, pos[0] + halfWidth, pos[1] + halfHeight, _water_level.getZ(), Data)
        waterBase = WaterBase(nodeName=Data['Name'], nodePath=water.waterNP, describeDict=Data,
                              plane=water.waterPlane, cam=water.watercamNP)        
        self.processTags(Data, waterBase)  
        return waterBase
    
    
    
class WaterBase(GameObject):
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        self.waterPlane = kwargs['plane']
        self.camNP = kwargs['cam']       
            
    def clone(self):
        clone = GameObject.clone(self)
        clone.camNP = self.camNP
        return clone
    
    def start(self):
        taskMgr.add(self.controlCamera, "update water")
        
    def stop(self):
        taskMgr.remove("update water")
        
    def controlCamera(self, task):
        mc = base.camera.getMat(render)
        mf = self.waterPlane.getReflectionMat()
        self.camNP.setMat(mc * mf)
        self.nodePath.setShaderInput('time', task.time / 5)        
        return task.cont    

class WaterNode():
    def __init__(self, x1, y1, x2, y2, z, Data):
        print('setting up water plane at z=' + str(z))
        
        # Water surface
        maker = CardMaker('water')
        maker.setFrame(x1, x2, y1, y2)

        self.waterNP = NodePath(maker.generate())
        self.waterNP.setHpr(0, -90, 0)
        self.waterNP.setPos(0, 0, z)
        self.waterNP.setTransparency(TransparencyAttrib.MAlpha)
        self.waterNP.setShader(loader.loadShader(Data['WaterShaderPath']))
        self.waterNP.setShaderInput('wateranim', Vec4(0.3, -0.15, 48.0, 10))  # vx, vy, scale, skip
        # offset, strength, refraction factor (0=perfect mirror, 1=total refraction), refractivity
        self.waterNP.setShaderInput('waterdistort', Vec4(0.4, 4.0, 0.25, 0.7))    
        self.waterNP.setShaderInput('time', 0)
        # Reflection plane
        self.waterPlane = Plane(Vec3(0, 0, z + 1), Point3(0, 0, z))
        
        planeNode = PlaneNode('waterPlane')
        planeNode.setPlane(self.waterPlane)
        
        # Buffer and reflection camera
        buffer = base.win.makeTextureBuffer('waterBuffer', 512, 512)
        buffer.setClearColor(Vec4(0, 0, 1, 1))

        cfa = CullFaceAttrib.makeReverse()
        rs = RenderState.make(cfa)

        self.watercamNP = base.makeCamera(buffer)        
        self.watercamNP.reparentTo(render)
        
        sa = ShaderAttrib.make()
        sa = sa.setShader(loader.loadShader(Data['splut3ClippedShaderPath']))

        cam = self.watercamNP.node()
        cam.getLens().setFov(base.camLens.getFov())
        cam.getLens().setNear(0.01)
        cam.getLens().setFar(500000)
        cam.setInitialState(rs)
        cam.setTagStateKey('Clipped')
        cam.setTagState('True', RenderState.make(sa)) 

        # ---- water textures ---------------------------------------------

        # reflection texture, created in realtime by the 'water camera'
        tex0 = buffer.getTexture()
        tex0.setWrapU(Texture.WMClamp)
        tex0.setWrapV(Texture.WMClamp)
        ts0 = TextureStage('reflection')
        self.waterNP.setTexture(ts0, tex0) 

        # distortion texture
        tex1 = loader.loadTexture(Data['WaterTexturePath'])
        ts1 = TextureStage('distortion')
        self.waterNP.setTexture(ts1, tex1)
        
        distorShaderInput = z - 0.05
        print "water level", distorShaderInput
        render.setShaderInput('waterlevel', distorShaderInput)         
        
        self.waterNP.setColor(0, 0, 1, 1)
