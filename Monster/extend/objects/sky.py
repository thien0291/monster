from Monster.core.objects.baseobject import GameObject
from panda3d.core import BitMask32, Vec4, Vec3, Texture, TextureStage, CardMaker, TransparencyAttrib, NodePath
from panda3d.core import TexGenAttrib
import sys, copy
from Monster.utility import caster
from Monster.extend.objects.sun import *
from Monster.core.objects.map import Map
import math
from Monster.utility.pritority import Priority
from Monster.core.objects.baseBuilder import BaseBuilder


class SkyBuilder(BaseBuilder):
        
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Skys'][Data['objectID']]
        sky = prototype.clone()
        sky.nodePath.reparentTo(nodeRoot)
        return sky
    
    def buildSkyDome(self, Data):
        modelPath = Data['Model']           
        textureFile = Data['TextureFile']
        
        name = Data['Name']
                        
        shaderFile = Data['ShaderFile']
        shaderInputs = Data['ShaderInputs']
        
        scale = Vec3(*caster.stringListToListFloat(Data['Scale'].split(',')))
        
        height = caster.stringToFloat(Data['ZUnderZero'], 0)        
        
        speed = caster.stringToFloat(Data['CloudSpeed'], 1)
        
        skydome = SkyDome(name, scale, modelPath, textureFile, shaderFile, height, speed, shaderInputs)
        
        sky = SkyMonoLayer(skydome, nodeName=name, nodePath=skydome.dome, describeDict=Data)
        self.processTags(Data, sky)  
        return sky
    
    def buildSkyMulti(self, Data):
        # print "build sky multi"
        skyNode = NodePath(Data['Name'])
        skybox = None
        cloud = None
        name = Data['Name']
        if Data.has_key('SkyBox'):            
            skyboxData = Data['SkyBox']
            skyboxModel = skyboxData['Model']
            skyboxScale = caster.stringToFloat(skyboxData['Scale'], 1000)                      
            skybox = SkyBox(name, skyboxModel, skyboxScale)
            skybox.box.reparentTo(skyNode)            
            if skyboxData.has_key('Depth'):
                skybox.setDepth(eval(skyboxData['Depth']))  
        if Data.has_key('Cloud'):
            cloudData = Data['Cloud']
            cloudModel = cloudData['Model']
            cloudTexture = cloudData['Texture']
            cloudScale = Vec3(*caster.stringListToListFloat(cloudData['Scale'].split(','), 10000))
            cloudHeight = caster.stringToFloat(cloudData['Height'], -9600)
            cloudSpeed = caster.stringToFloat(cloudData['Speed'], 0.03)
            cloud = CloudLayer(name, cloudModel, cloudTexture, cloudScale, cloudHeight, cloudSpeed)
            cloud.clouds.reparentTo(skyNode)
            
        sky = SkyMultiLayer(skybox, cloud, nodeName=name, nodePath=skyNode, describeDict=Data)
        self.processTags(Data, sky)  
        return sky
    
    def buildSkybox(self, Data):
        modelPath = Data['Model']           
        textureFile = Data['TextureFile']        
        name = Data['Name']                                  
        scale = Vec3(*caster.stringListToListFloat(Data['Scale'].split(','), 100))
        
        skybox = SkyBox(name, modelPath, scale, textureFile)
        if Data.has_key('Hpr'):
            hpr = Vec3(*caster.stringListToListFloat(Data['Hpr'].split(',')))
            skybox.box.setHpr(hpr)

        if Data.has_key('Far'):
            skybox.setFar(caster.stringToFloat(Data['Far'], 0))
        if Data.has_key('Height'):
            skybox.setHeight(caster.stringToFloat(Data['Height'], 0))
        if Data.has_key('Depth'):
            skybox.setDepth(eval(Data['Depth']))
        sky = SkyMonoLayer(skybox, nodeName=name, nodePath=skybox.box, describeDict=Data)
        self.processTags(Data, sky)  
        return sky
    
    def buildType(self, Data):
        if Data['Type'].lower() == "skymonolayer" or  Data['Type'].lower() == "skydome":
            return self.buildSkyDome(Data)
        elif Data['Type'].lower() == "skymultilayer":
            return self.buildSkyMulti(Data)
        elif Data['Type'].lower() == "skybox":
            return self.buildSkybox(Data)
        

class SkyFactory():
    '''Factory pattern: use for creating sky in this module.'''
    @staticmethod
    def createSky(skyType, *args, **kwargs):
        model = getattr(sys.modules[__name__], skyType)(*args, **kwargs)
        return model
    
class SkyBase(GameObject):
    pass



class ColoredByTime():
    def __init__(self, dayColor, nightColor, sunsetColor):
        self.dayColor = dayColor  # Vec4(0.8, 0.9, 0.95, 1.0)
        self.nightColor = nightColor  # Vec4(-0.5, -0.3, .0, 1.0)
        self.sunsetColor = sunsetColor  # Vec4(0.75, .60, .65, 1.0)
        self.schedule = ((14400, self.nightColor), (28800, self.sunsetColor),
                         (32400, self.dayColor), (54000, self.dayColor),
                         (57600, self.sunsetColor), (72000, self.nightColor))

    def interpolateColor (self, start, end, time, startColor, endColor):
        ratio = (time - start) / (end - start + 0.000001)
        self.setColor(endColor * ratio + startColor * (1 - ratio))

    def colorize(self, time):
#        print "TIME : ", time
        lastPair = self.schedule[-1]
        for pair in self.schedule:
            if pair[0] > time:
                self.interpolateColor(pair[0], lastPair[0], time, pair[1], lastPair[1])
                break
            lastPair = pair
            

class CustimizeColoredByTime():
    def __init__(self, schedule):        
#        self.dayColor = dayColor # Vec4(0.8, 0.9, 0.95, 1.0)
#        self.nightColor = nightColor # Vec4(-0.5, -0.3, .0, 1.0)
#        self.sunsetColor = sunsetColor # Vec4(0.75, .60, .65, 1.0)
        self.lastColor = Vec4(0, 0, 0, 1)
        self.schedule = schedule

    def interpolateColor (self, start, end, time, startColor, endColor):
        ratio = (time - start) / (end - start + 0.000001)
#        self.setColor(endColor * ratio + startColor * (1 - ratio))
        return (endColor * ratio + startColor * (1 - ratio))

    def colorize(self, time):
#        print "TIME : ", time
        lastPair = self.schedule[-1]
        for pair in self.schedule:
            if pair[0] > time:
                self.lastColor = self.interpolateColor(pair[0], lastPair[0], time, pair[1], lastPair[1])
                break
            lastPair = pair
        return self.lastColor

          

class SkyMonoLayer(SkyBase):    
    def __init__(self, sky, *args, **kwargs):
        SkyBase.__init__(self, *args, **kwargs)
        if isinstance(sky, SkyDome):
            self.skydome = sky
        else:
            self.skybox = sky
        
    def start(self):
        taskMgr.add(self.update, "skymonolayer update", sort=Priority.Sky)
        
    def stop(self):
        taskMgr.remove("skymonolayer update")
             
    def update(self, task):        
        if hasattr(self, 'skydome'):
            pos = base.camera.getPos(render) 
            self.nodePath.setPos(pos)
            pos.setZ(pos.getZ() + self.skydome.height)
            self.nodePath.setShaderInput('time', task.time / self.skydome.speed)
        elif hasattr(self, 'skybox'):
            self.skybox.update()
        return task.cont

class SkyMultiLayer(SkyBase):
    def __init__(self, skybox, cloud, *args, **kwargs):
        SkyBase.__init__(self, *args, **kwargs)
        self.skybox = skybox
        self.cloud = cloud
        self.dateTime = DayTime(32800, 1)
        self.dayColor = Vec4(0.98, 0.98, 0.85, 1.0)
        self.nightColor = Vec4(0, 0, 0.0, 1.0)
        self.sunsetColor = Vec4(1, 1, 0, 1.0)
        directLightSchedule = ((10800, self.nightColor), (28800, self.sunsetColor),
                         (32400, self.dayColor), (54000, self.dayColor),
                         (57600, self.sunsetColor), (61200, self.nightColor))

        self.directLightColor = CustimizeColoredByTime(directLightSchedule)
        self.dayColor = Vec4(1, 1, 1, 1.0)
        self.nightColor = Vec4(0.2, 0.2, 0.2, 1.0)
        self.sunsetColor = Vec4(0.5, 0.5, 0.5, 1.0)
        
        ambientLightSchedule = ((10800, self.nightColor), (28800, self.sunsetColor),
                         (32400, self.dayColor), (54000, self.dayColor),
                         (57600, self.sunsetColor), (61200, self.nightColor))

        self.ambientLightColor = CustimizeColoredByTime(ambientLightSchedule)
        
    def clone(self):
        clone = GameObject.clone(self)        
        node = NodePath("skymulti")        
        clone.cloud = self.cloud.clone()
        clone.cloud.clouds.reparentTo(node)
        clone.skybox.box.reparentTo(node)
        clone.nodePath = node        
        return clone
    
    def start(self):
        taskMgr.add(self.update, "skymultilayer update ", sort=Priority.Sky)
        
    def stop(self):
        taskMgr.remove("skymultilayer update ")
             
    def update(self, task):                
        elapsed = globalClock.getDt()
#         # cloud
        self.cloud.update(elapsed)
#         # box        
        self.skybox.update()
#         self.dateTime.update(elapsed)
# #        time = self.dateTime.hour * 100 + self.dateTime.minute / 6 * 10 + self.dateTime.second / 6 
#         time = self.dateTime.totalTime
#         currentMap = Map.current
#         if currentMap.gameObjects.has_key('Lights'):
#             for key, light in currentMap.gameObjects['Lights'].items():
#                 if isinstance(light.node(), DirectionalLight):
#                     light
#                     start = light.getP()
#                     end = -(time/240 - 90)
#                     ratio = elapsed
#                     p = end * ratio + start * (1 - ratio)
#                     light.setP(p)
#     #                print " HPR : ", type(light), " ", light.nodePath.getHpr()
#                     light.setColor(self.directLightColor.colorize(time % DayTime.TOTAL_SECOND_IN_DAY))
#     #                print "DirectLight Color by time : ", self.directLightColor.colorize(self.dateTime.hour * 100 + self.dateTime.minute / 6 * 10 + self.dateTime.second / 6)
#                 else:
#                     light
#                     light.setColor(self.ambientLightColor.colorize(time % DayTime.TOTAL_SECOND_IN_DAY))
        
               
#         self.cloud.colorize(time% DayTime.TOTAL_SECOND_IN_DAY)
#         self.skybox.colorize(time% DayTime.TOTAL_SECOND_IN_DAY)
        return task.cont
        
class SkyDome():
    def __init__(self, name, scale, modelPath, texturePath, shaderPath, height, speed, shaderInputs):
        self.height = height
        self.speed = speed        
        self.dome = base.loader.loadModel(modelPath)
        texture = base.loader.loadTexture(texturePath)
        textureStage0 = TextureStage("dome ts " + name)
        textureStage0.setMode(TextureStage.MReplace)        
        self.dome.setTexture(textureStage0, texture, 1)        
        self.dome.setShader(base.loader.loadShader(shaderPath))
        self.dome.setShaderInput('time', 0)
        for key, value in shaderInputs.items():       
            inputValue = []
            for item in value.split(','):                
                if caster.isFloat(item):
                    item = float(item)
                inputValue.append(item)         
            self.dome.setShaderInput(key, *inputValue)                            
        self.dome.setScale(scale)
        self.dome.setBin('background', 0)


        

class CloudLayer(ColoredByTime):
    def __init__(self, name, modelPath, texturePath, scale, height, speed):
        tex1 = loader.loadTexture(texturePath)
        tex1.setMagfilter(Texture.FTLinearMipmapLinear)
        tex1.setMinfilter(Texture.FTLinearMipmapLinear)
        tex1.setAnisotropicDegree(2)
        tex1.setWrapU(Texture.WMRepeat)
        tex1.setWrapV(Texture.WMRepeat)
        tex1.setFormat(Texture.FAlpha)
        self.ts1 = TextureStage('clouds')     
        self.sphere(scale, height)

        self.clouds.setTransparency(TransparencyAttrib.MDual)
        self.clouds.setTexture(self.ts1, tex1)
        
        self.clouds.setBin('background', 2)
        # self.clouds.setDepthWrite(False)
        self.clouds.setDepthTest(False)
        self.clouds.setTwoSided(True)
        # self.clouds.setLightOff(1)
        self.clouds.setShaderOff(1)
        # self.clouds.setFogOff(1)        

        self.speed = speed
        self.time = 0
#        self.dayColor = Vec4(1, 1, 1, 1.0)
#        self.nightColor = Vec4(0, 0, .0, 1.0)
#        self.sunsetColor = Vec4(0.7, .75, .85, 1.0)
        self.blackColor = Vec4(0, 0, 0, 1)
        
        self.dayColor = Vec4(0.98, 0.98, 0.95, 1.0)
        self.nightColor = Vec4(-0.5, -0.3, .0, 1.0)
        self.sunsetColor = Vec4(0.75, .60, .65, 1.0)
        
        self.clouds.setColor(Vec4(1, 1, 1, 1))
        ColoredByTime.__init__(self, self.dayColor, self.nightColor, self.sunsetColor)
    
    def setColor(self, color):
        self.clouds.setColor(color)     
    
    def clone(self):
        clone = copy.deepcopy(self)
        clone.ts1 = self.ts1
        return clone

    def sphere(self, scale, height):
        self.height = height
        self.clouds = loader.loadModel("Models/Nature/Sky/sphere")        
        self.clouds.setHpr(0, 90, 0)
        self.clouds.setScale(scale)
        self.clouds.setTexOffset(self.ts1, 0, 1)
        self.clouds.setTexScale(self.ts1, 30, 12)
                
    def update(self, elapsed):        
        self.time += elapsed
        self.clouds.setTexOffset(self.ts1, self.time * self.speed, self.time * self.speed)
        self.clouds.setPos(base.camera.getPos(render) + Vec3(0, 0, self.height))
                

class SkyBox(ColoredByTime):
    def __init__(self, name, modelPath, scale, cubePath=None):        
        self.box = loader.loadModel(modelPath)
        if cubePath:
            tex = loader.loadCubeMap(cubePath)
            self.box.setTexGen(TextureStage.getDefault(), TexGenAttrib.MWorldCubeMap)
            self.box.setTexture(tex)
        else:
            self.box.setTextureOff(1)    
        # self.box.setShaderOff(1)
        self.box.setTwoSided(True)        
        self.box.setScale(scale)
        self.box.setBin('background', 0)
#        self.box.setDepthWrite(False)
        
        # self.box.setDepthTest(False)
        self.box.setLightOff(1)        
        # self.box.setFogOff(1)
        # self.box.hide(BitMask32.bit(2)) # Hide from the volumetric lighting camera

#        self.dayColor = Vec4(0.8, 0.9, 0.95, 1.0)
#        self.nightColor = Vec4(-0.5, -0.3, .0, 1.0)
#        self.sunsetColor = Vec4(0.6, 0.675, 0.7125, 1.0)
#        
        self.dayColor = Vec4(.55, .65, .95, 1.0)
        self.nightColor = Vec4(.05, .05, .20, 1.0)
        self.sunsetColor = Vec4(.45, .5, .65, 1.0)
        
        self.box.setColor(self.sunsetColor)
        ColoredByTime.__init__(self, self.dayColor, self.nightColor, self.sunsetColor)

        self.height = 0
        self.far = 0

    def setDepth(self, value):
        self.box.setDepthTest(value)

    def setFar(self, far):
        self.far = far

    def setHeight(self, height):
        self.height = height
        
    def setColor(self, color):
        self.box.setColor(color)
        
    def update(self):
        forward = render.getRelativeVector(base.camera, Vec3(0, 1, 0))
        self.box.setPos(base.camera.getPos(render) + forward * self.far + self.height)

