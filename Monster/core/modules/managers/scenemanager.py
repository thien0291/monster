'''
Created on Apr 20, 2014

@author: thinhtran
'''


from direct.showbase.DirectObject import DirectObject
from direct.directnotify.DirectNotify import DirectNotify
from Monster.utility.io import XmlIO
from Monster.core.objects.gamenode import SceneNode

class SceneManager(DirectObject):
    '''
    Manage scenes and scene's switching.
    '''
    
    notify = DirectNotify().newCategory("Monster.core.modules.managers.scenemanager.SceneManager")
    
    def __init__(self):
        '''
        Constructor
        '''
        self.scenes = []
        self.current = None
        
        # Async loading
        self._refreshAsyncStatus()
        self.accept('asyncLoadedPart',self._asyncLoadedPart)
        
    def _refreshAsyncStatus(self):
        self.targetScene = None
        self.totalNode = 0
        self.loadedNode = 0
        
    def active(self, sceneNode):
        if isinstance(sceneNode.getPythonTag("node"), SceneNode):  
            SceneManager.notify.debug("SceneManager.active: " + sceneNode.getName())            
            self.current = sceneNode
            if not sceneNode in self.scenes:
                self.scenes.append(sceneNode)
                
            if self.current:
                self.current.detachNode()
                
            sceneNode.reparentTo(render)
            
            return True
        else:
            print "This is not SceneNode"
            
            
    def finishLoad(self):
        pass
            
    def _asyncLoadedPart(self, node):
        SceneManager.notify.debug("SceneManager._asyncLoadedPart: " + node.getName())        
        self.loadedNode += 1
        if self.loadedNode >= self.totalNode:            
            if self.targetScene:
                self.active(self.targetScene) 
                           
            self._refreshAsyncStatus()
            
    def activeWithLoadScene(self, loadSceneXml, targetXml):
        self._refreshAsyncStatus()
        
        # Active Loading Scene
        loadScene = SceneNode.create(loadSceneXml)
        self.active(loadScene)        
        
        # Count parts for loading background        
        io = XmlIO(targetXml)
        self.totalNode = io.countNode()
        
        # Loading target with background
        self.targetScene = SceneNode.create(targetXml, async=True)
        
        
        
    
        
        
    
        