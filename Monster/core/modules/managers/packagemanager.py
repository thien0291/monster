'''
Created on May 8, 2014

@author: thinhtran
'''

from Monster.core.modules.shortcut import getFullPath

class PackageManager(DirectObject):
    def __init__(self):        
        self.extendFolders = {"extend":"extend"}
        
        
    def _dynamicImport(self, location):
        folder = getFullPath(location)
        if sys.path.count(folder) == 0:                
            sys.path.append(folder)
        
    
    def refresh(self):
        packages = self.getList()
        for packageName in packages:
            self._dynamicImport(self.extendFolders[packageName])
        
    def install(self, packageName, packageURL):
        pass
    
    def remove(self, packageName):
        pass
    
    def publish(self, repositoryURL):
        pass
    
    def getList(self):
        return self.extendFolders.keys()
    
    
    
        