#import from python lib
import sys
import json
import time
#Import from Panda3d
from Monster.utility.io import XmlIO
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from direct.stdpy import thread
from direct.interval.IntervalGlobal import Sequence, Func, Wait
#Import from Framework
from Monster.core.objects.map import Map
from Monster.core.modules.managers.manager import *
from Monster.core.modules.factories.inputFactory import InputFactory
from Monster.core.modules.factories.GUIFactory import GuiFactory
from Monster.core.modules.factories.mapFactory import MapFactory
from Monster.core.modules.factories.buildTypeFactory import BuildTypeFactory
from Monster.extend.modules.builders.builder import *
from Monster.core.modules.factories.factory import *
from Monster.core.modules.managers.gui import GuiManager

class MapManager(object):
    current = None
    
    def __init__(self):                
        self.mapList = {} #Dict: contain all complete map which really be loaded by factory
        self.currentIdx = 0 #Int: current index of map (of self.mapList)
        self.previous = None #previous Loaded Map
        self.currentMap = None #current Loaded Map
        MapManager.current = self                   
        
    def loadBulletWorld(self, data):
        """
        @summary: load bullet world from input data
        @param data: Dict - World data (Gravity define)
        @return: BulletWorld Object 
        """
        world = BulletWorld()
        if data.has_key('Gravity'):
            gravity = caster.stringListToListFloat(data['Gravity'].split(','), 0)
        else:
            gravity = [0, 0, -9.81]
        world.setGravity(Vec3(*gravity))
        return world
        
    def loadMapListFromFile(self, filename):
        """
        @summary: virtual func
        """
        pass
        
    def getCurrentMap(self):
        """
        @summary: get current map
        @return: Map 
        """
        return self.mapList.values()[self.currentIdx]
        
    def nextMap(self):
        """
        @summary: go to next map
        """
        idx = self.currentIdx + 1
        self.goToMap(idx)
        
    def goToMap(self, idx):
        pass
    

        
class TotalMapManager(MapManager):
    def loadMapListFromFile(self, filename):
        xmlIn = XmlIO(filename)
        xmlData = xmlIn.readFile()        
        
        prototype = xmlData['Game']['Prototypes'].values()[0]
        prototypePath = prototype['Path']
        BuildTypeFactory.product(filename=prototypePath)
        
        xmlData = xmlData['Game']['Maps']    
        
        
        for key, value in xmlData.items():            
            world = None
            if value.has_key('World'):
                world = self.loadBulletWorld(value['World'])            
            self.mapList[key] = MapFactory.product(filename=value['Path'], world=world)
            if value.has_key('Input'):
                inputs = InputFactory.product(filename=value['Input'])
                self.mapList[key].addInput(*inputs)
            if value.has_key('Gui'):
                gui = GuiFactory.product(filename=value['Gui'])
    
        self.mapList.values()[self.currentIdx].applyToScene(render)
        self.mapList.values()[self.currentIdx].active()

    def goToMap(self, idx):
        self.previous = self.getCurrentMap()
        self.getCurrentMap().rejectFromScene()
        self.currentIdx = idx
        self.currentIdx = self.currentIdx % len(self.mapList)
        print "Next map in Idx", self.currentIdx
        self.getCurrentMap().applyToScene(render)
        
class CacheMapManager(MapManager):
    """
    @summary: This is a type of Map Manager that will build only used prototype and put it to a self-cache (not all prototype like TotalMapManager) 
    """
    def __init__(self):
        self.items = None #Dict: All maps that be load from xml 
        self.maps = None #List: All maps value that load from XML file
        self.keys = None #List: All key of map that reference to the map data (self.maps)
        self.prototypePath = None #string: The path to prototype XML files
#         self.mapList = {};#Dict: contain all complete map which really be loaded by factory
#         self.currentIdx = 0;#Int: current index of map (of self.mapList)
        MapManager.__init__(self)
        
     
    
    def getCurrentMap(self):
        """
        @summary: get current map
        @return: index of current map
        """
        return self.currentMap
    
    def getIdList(self, filename):
        """
        @summary: get list of prototype element of file input
        @param filename: (string) path to file
        @return: list of prototype 
        """
        xmlIn = XmlIO(filename)
        return xmlIn.getAllElementsTextByTagName('Prototype')
        
    def loadMapListFromFile(self, filename, idx=0): 
        """
        @summary: load map (game) from an xml file input
        @param filename: (string) path to xml file input
        @param idx: (int) the index of map that will be load. default = 0;  
        """
        # Read map list xml file
        xmlIn = XmlIO(filename)
        xmlData = xmlIn.readFile()      
        # Read map info
        mapsData = xmlData['Game']['Maps']
        self.maps = mapsData.values()
        self.keys = mapsData.keys()
        self.items = mapsData
        mapsLen = len(self.maps)        
        if isinstance(idx, str):
            key = idx
            idx = self.keys.index(key)  
        idx = idx % mapsLen        
        mapLoad = self.maps[idx]
        key = self.keys[idx]
        prototypeData = xmlData['Game']['Prototypes'].values()[0]
        self.prototypePath = prototypeData['Path']
        if mapLoad.has_key('Path'):
            mapPath = mapLoad['Path']
            world = None
            idList = self.getIdList(mapPath)        
            # Initialize prototypes only for map with idx == 0                  
            
            BuildTypeFactory.product(filename=self.prototypePath, idList=idList)                
            # Load map
            if mapLoad.has_key('World'):
                world = self.loadBulletWorld(mapLoad['World'])            
            self.mapList[key] = MapFactory.product(filename=mapPath, world=world)
        else:
            self.mapList[key] = Map()    
        if mapLoad.has_key('Input'):
            inputs = InputFactory.product(filename=mapLoad['Input'])
            self.mapList[key].addInput(*inputs)
            
        if mapLoad.has_key('Gui'):
            gui = GuiFactory.product(filename=mapLoad['Gui'])
            self.mapList[key].setGui(gui)
            gui.rejectFromScene()
            
        self.currentIdx = 0     
        if self.mapList.has_key(key):
            self.currentMap = self.mapList[key]
        # Apply map to scene
        self.mapList.values()[self.currentIdx].applyToScene(render)
        self.mapList.values()[self.currentIdx].active()
                
    def goToWaitMap(self, waitmap_name, next_map_name, delay=0.5):
        """
        @summary: show wait map and load next map (do as a panda3d sequence).
        @param waitmap_name: (str) name of wait map
        @param next_map_name: (str) name of next map will be load
        @param delay: (float) waiting time.   
        """
        s = Sequence(
                 Func(self.goToMap, waitmap_name),
                 Wait(delay),
                 Func(self.goToMap, next_map_name))        
        s.start()
        
                
    def getIdxFromName(self, map_name):
        """
        @summary: get index(int) in self.maps of map with name
        @return: map index (int)
        """
        return self.keys.index(map_name) 
    
    def initPrototype(self, idList):
        """
        @summary: init all prototype that define in idList.
        @param idList: (List) list of prototype Id that will be loaded 
        """
        BuildTypeFactory.product(filename=self.prototypePath, idList=idList)  # Load map

    def loadMap(self, map_name):        
        """
        @summary: load all element in map with map's name include GUI, Input, World, etc
        @param map_name: (str) name of map will be loaded
        """
        self.previous = self.getCurrentMap()
        idx = self.keys.index(map_name) 
        self.currentIdx = idx
        self.currentIdx = self.currentIdx % len(self.maps)
        mapLoad = self.maps[self.currentIdx]
        map_name = self.keys[self.currentIdx]
        print "Cache: Next map", self.mapList, map_name
        if not self.mapList.has_key(map_name):
            if mapLoad.has_key('Path'):
                mapPath = mapLoad['Path']
                idList = self.getIdList(mapPath)
                print "Cache: Next map in Idx", self.currentIdx  # Initialize prototypes only for map with idx == 0
                BuildTypeFactory.product(filename=self.prototypePath, idList=idList)  # Load map
                world = None
                if mapLoad.has_key('World'):
                    world = self.loadBulletWorld(mapLoad['World'])
                self.mapList[map_name] = MapFactory.product(filename=mapPath, world=world)
            else:
                self.mapList[map_name] = Map()
            if mapLoad.has_key('Input'):
                inputs = InputFactory.product(filename=mapLoad['Input'])
                self.mapList[map_name].addInput(*inputs)
            if mapLoad.has_key('Gui'):
                gui = GuiFactory.product(filename=mapLoad['Gui'])
                self.mapList[map_name].setGui(gui)
                gui.rejectFromScene()
        
        if self.mapList.has_key(map_name):
            self.currentMap = self.mapList[map_name]
            

    def goToMap(self, idx):
        """
        @summary: go to map with map_name (include reject current scene)
        @param idx: (str) name of map 
        """
        self.getCurrentMap().rejectFromScene()                
        self.loadMap(idx)
        # Apply map to scene
        self.getCurrentMap().applyToScene(render)