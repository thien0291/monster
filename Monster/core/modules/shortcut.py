import os
from panda3d.core import Filename, Vec3


def getFullPath(dynamicPath):
    workingDir = os.getcwd()        
    workingDir = Filename.fromOsSpecific(workingDir).getFullpath()
    # create path for panda3d file system
    return workingDir + dynamicPath


def getForward(np):
    return render.getRelativeVector(np, Vec3(0, 1, 0))


def getGameNode(nodePath):
    return nodePath.getPythonTag("node")