'''All Factory need in game...:-s
'''
import sys
from Monster.utility.io import XmlIO
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from Monster.core.objects.map import Map
from Monster.core.modules.managers.manager import *
from Monster.extend.modules.builders.builder import *
import json
from Monster.core.modules.managers.gui import GuiManager
import time
from direct.stdpy import thread

from direct.interval.IntervalGlobal import Sequence, Func, Wait


class BaseFactory():
    '''Abstract Factory for creating game.'''
    @staticmethod
    def product(*args, **kwargs):
        pass
    