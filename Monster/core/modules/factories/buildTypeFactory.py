#import from python
import sys
import json
import time
#import from Panda3d
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from direct.stdpy import thread
from direct.interval.IntervalGlobal import Sequence, Func, Wait
#import from Framework
from Monster.core.modules.factories.factory import BaseFactory
from Monster.core.objects.map import Map
from Monster.core.modules.managers.manager import *
from Monster.extend.modules.builders.builder import *
from Monster.utility.io import XmlIO


class BuildTypeFactory(BaseFactory):
    @staticmethod
    def product(*args, **kwargs):
        """
        @summary: (static method) build all prototype that's define from XML file (all or specified in a IdList)
        @param *argv: keyword (filename = (str) file path, idList = (List) list of prototype Id will be loaded) 
        @param filename: (*require) (string) file path
        @param idList: (*option) (List) list of prototype will be loaded.
        @return: game.  
        """
        filename = kwargs['filename']
        idList = None
        if kwargs.has_key('idList'):
            idList = kwargs['idList']            
        xmlIn = XmlIO(filename)
        data = xmlIn.readFile()
        # print data
        data = data['Game']
        for key, value in data.items():
            key = key.replace('Path', '')
            managerType = key[0:len(key)] + "Manager"
            managerClass = getattr(sys.modules[__name__], managerType)
            manager = managerClass()            
            manager.InitAllPrototype(value, idList)
            managerClass.prototypeList = manager.prototypeList
        
        game = GameEntityManager.objectList
        return game
    
