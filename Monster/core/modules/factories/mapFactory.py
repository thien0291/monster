#import from python
import sys
import json
import time
#import from Panda3d
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from direct.stdpy import thread
from direct.interval.IntervalGlobal import Sequence, Func, Wait
#import from Framework
from Monster.core.modules.factories.factory import BaseFactory
from Monster.core.objects.map import Map
from Monster.core.modules.managers.manager import *
from Monster.extend.modules.builders.builder import *
from Monster.utility.io import XmlIO


class MapFactory(BaseFactory):    
    @staticmethod
    def product(*args, **kwargs):    
        """
        @summary: load map from xml file.
        @param *kwargs: define by keywords (filename, world)
        @param filename: (*require) XML file path
        @param world: (optional) World object. If not define, a world with -9.81 gravity will be create
        @return: Map object.
        """
        filename = kwargs['filename']      
        if kwargs.has_key('world') and kwargs['world']:
            world = kwargs['world']
        else:
            world = BulletWorld()
            world.setGravity(Vec3(0, 0, -9.81))
        # nodeRoot = NodePath(filename)
          
        game = GameEntityManager.objectList
        print game
        xmlMap = XmlIO(filename)
        mapData = xmlMap.readFile()
        
        nodeRoot = NodePath(PandaNode("GameRoot"))
        
        mapElement = {}
        gameNode = mapData['Game']
        for key, value in gameNode.items():            
            gameObjects = {}   
            if isinstance(value, dict):
                for objectType, objectData in value.items():
                    builderType = key[0:-1] + "Builder"
                    builder = getattr(sys.modules[__name__], builderType)()
                    id = objectData['Prototype']
                    objectData['objectID'] = id  
                    objectData['Name'] = objectType                    
                    object = game[key][id]                          
                    gameObject = builder.buildGameObject(objectData, game, nodeRoot, world)
                    gameObjects[objectType] = gameObject    
                mapElement[key] = gameObjects
        # Check fog
        if not mapElement.has_key('Fogs') or len(mapElement['Fogs']) == 0:
            nodeRoot.setShaderInput('fog_mode', -1)
            
        # Check MapType
        if gameNode.has_key('MapType'):
            m = getattr(sys.modules[__name__], gameNode['MapType'])(mapElement, world, CameraBase.current)
        else:   
            m = Map(mapElement, world, CameraBase.current)   
        
        # Clear current camera
        CameraBase.clearCurrent()     
        
        # Set nodeRoot to map
        m.nodeRoot = nodeRoot
        
        # Check shadow  
        shadow = nodeRoot.getPythonTag("Shadow")        
        if shadow:
            print "Enable Shadow"
            m.useShadowShader(shadow)
        else:
            m.useCommonShader()
        
        return m