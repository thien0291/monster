#import from python
import sys
import json
import time
#import from Panda3d
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from direct.stdpy import thread
from direct.interval.IntervalGlobal import Sequence, Func, Wait
#import from Framework
from Monster.core.modules.factories.factory import BaseFactory
from Monster.core.objects.map import Map
from Monster.core.modules.managers.manager import *
from Monster.extend.modules.builders.builder import *
from Monster.core.modules.managers.gui import GuiManager
from Monster.utility.io import XmlIO

class GuiFactory(BaseFactory):
    
    @staticmethod
    def recursive(guiMgr, key, value):        
        lst_gui = {}
        for gui_var_name, gui_data in value.items():
            guiBuilderType = key + "GuiBuilder"            
            builder = getattr(sys.modules[__name__], guiBuilderType)()
            
            # Process inherit data            
            data_load = builder.processInherit(gui_data)
            if gui_data.has_key('childs') and data_load.has_key('childs'):                
                data_load['childs'].update(gui_data['childs'])
                gui_data.pop('childs')
            data_load.update(gui_data)  
            gui_data = data_load
            
            lst_child = {}
            if gui_data.has_key("childs"):
                childs_data = gui_data["childs"]
                gui_data.pop("childs")
                lst_child = GuiFactory.recursive(guiMgr, key, childs_data)
                            
            if guiMgr.existGui(gui_var_name):
                guiMgr.removeGui(gui_var_name)           
            gui_object = builder.buildGui(guiMgr, gui_var_name, gui_data)
            lst_gui[gui_var_name] = gui_object
            guiMgr.childs[gui_var_name] = gui_object
            guiMgr.parents[gui_var_name] = {"gui" : gui_object, "childs" : lst_child}
            for child in lst_child.values():
                child.reparentTo(gui_object)
        return lst_gui
            
    @staticmethod
    def product(*args, **kwargs):        
        """
        @summary: load GUI from xml file.
        @param *kwargs: define by keywords (filename, gui, data)
        @param filename: (*option) (str) XML file path if not define, you have to define data keyword
        @param data: (*option) (dict) all data are define in a dict
        @param gui: (*option) GuiManager Object, If not define, a GuiManager Object will be created.
        @return: GuiManager Object
        """
        if kwargs.has_key('gui'):
            guiMgr = kwargs['gui']
        else:
            guiMgr = GuiManager()
        if kwargs.has_key('filename'):
            filename = kwargs['filename']  
            json_data = open(filename)
            data = json.load(json_data)
        else:
            data = kwargs['data']
        for key, value in data.items():            
            if isinstance(value, dict):
                lst_gui = GuiFactory.recursive(guiMgr, key, value)
                guiMgr.roots.update(lst_gui)   
        guiMgr.refresh()     
        return guiMgr   
        
