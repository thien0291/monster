#import from python
import sys
import json
import time
#import from Panda3d
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from direct.stdpy import thread
from direct.interval.IntervalGlobal import Sequence, Func, Wait
#import from Framework
from Monster.core.modules.factories.factory import BaseFactory
from Monster.core.objects.map import Map
from Monster.core.modules.managers.manager import *
from Monster.extend.modules.builders.builder import *
from Monster.utility.io import XmlIO

class InputFactory(BaseFactory):
    @staticmethod
    def product(*args, **kwargs):    
        """
        @summary: load Input from xml file.
        @param *kwargs: define by keywords (filename)
        @param filename: (*require) path to XML file
        @return: list of Input.
        """
        filename = kwargs['filename']      
        xmlMap = XmlIO(filename)
        inputData = xmlMap.readFile()
        inputNode = inputData['Input'] 
        inputs = []
        for key, value in inputNode.items():            
            if isinstance(value, dict):
                for objectType, objectData in value.items():
                    managerType = key + "InputBuilder"
                    builder = getattr(sys.modules[__name__], managerType)()
                    inp = builder.buildInput(objectType, objectData)
                    inputs.append(inp)
        return inputs
