'''
Created on Apr 18, 2014

@author: thinhtran
'''
import sys
import os
from panda3d.core import PandaNode, NodePath
from direct.directnotify.DirectNotify import DirectNotify

from Monster.utility import caster
from Monster.utility.io import XmlIO

class GameNode(PandaNode):
    """
    GameNode overrides PandaNode which is the core unit of Panda3D engine.
    Just like PandaNode, GameNode attributes doesn't affect to NodePath.
    We should use setPythonTag in the case we want to share attributes with NodePath.        
    """        
    notify = DirectNotify().newCategory("Monster.core.objects.gamenode.GameNode")
    
    def __init__(self, aName):
        PandaNode.__init__(self, aName)
        PandaNode.setPythonTag(self, "node", self)
        self.nodePath = NodePath(self)
        self._async = False
        
    def __getattr__(self, name):
        return getattr(self.nodePath, name)
        
    @staticmethod    
    def create(filePath, async=False):
        io = XmlIO(filePath)
        xmlRoot = io.getRoot()
        builderName = xmlRoot.get("builder")
        if builderName:
            builder = getattr(sys.modules[__name__], builderName)()
        else:
            builder = GameNodeBuilder()
        node = builder.make(xmlRoot, async)
        return node
        
        
    def _setAsync(self, value):
        """
        default: False
        
        self._async == True --> load Model performs asynchronously. This attribute won't affect after node is loaded.
        """
        self._async = value
        
    
    def toNodePath(self):
        """
        Get associated NodePath of this GameNode.
        Return:
            NodePath - core unit handler of Panda3D
        """  
        return self.nodePath
            
    
    def applyRecursive(self, functionName, extraArgs):
        """
        Call function recursive for GameNode. Priority for getting functionName: GameNode, NodePath
        ex: node.applyRecursive("setAsync", true)
        
        Params:
            functionName - name of function call (can be function of GameNode or NodePath)
            extraArgs - parameters to be passed through function call
        
        Return:
            None
        """
        # Recursive call
        if self.nodePath.getChildren():    
            for child in self.nodePath.getChildren():            
                childNode = child.getPythonTag("node")
                if childNode:                    
                    childNode.applyRecursive(functionName, extraArgs)
                    
        # Call functionName        
        func = getattr(self, functionName)
        func(*extraArgs)
    
    
    def asyncLoadFinished(self, loadedNodePath):  
        """
        This function is called when system loaded egg model. Then, notify to SceneManager for background loading mode
        """              
        GameNode.notify.debug("GameNode.asyncLoadFinished: " + self.getName())        
        loadedNodePath.instanceTo(self.nodePath)
        # Notify to SceneManager to manage background switching screens        
        messenger.send("asyncLoadedPart", [self])
        
        
    def refresh(self, optionDefs):    
        """
        Apply properties from list definitions to Panda3D NodePath
        Params:
            nodepath - NodePath: target NodePath need to be applied properties. It can be self.nodePath or other GameNode's NodePath
            optionDefs - list: list of properties for target NodePath
        Return:
            NodePath - target NodePath which applied properties
        """    
        
        for option in optionDefs:
            doFunc = False
            # list of parameters
            params = option[0] 
            # string of function name
            funcName = option[1]
            # get function call 
            if option[2] is not None:
                func = getattr(eval(option[2]), funcName)   
                if option[2] == "loader":    
                    if self._async == True:                
                        func(*params, callback=self.asyncLoadFinished)
                    else:                        
                        modelNode = func(*params)
                        modelNode.instanceTo(self.nodePath)
                    doFunc = True                
            else:                
                func = getattr(self, funcName)         
            # do function    
            if not doFunc:
                func(*params)
                        
        return self
    
class SceneNode(GameNode):
    pass
    
    
class GameNodeBuilder(object):
    """
    GameNodeBuilder based on lxml library, we must install requirements.txt before using this class.
    This class read lxml ElementTree and parse to GameNode object. We suppose some convention on XML file.
    Please read document to have a best understanding on XML syntax.
    """
    
    notify = DirectNotify().newCategory("Monster.core.objects.gamenode.GameNodeBuilder")
    
    def make(self, xmlRoot, async=False):
        """
        Currently, this function only create one NodePath based on current lxml Element.
        Return:
            NodePath - Associated notepath for GameNode. To retrieve our GameNode, please use notepath.node()
        """        
        GameNodeBuilder.notify.debug("GameNodeBuilder.make")
        if xmlRoot.get("class") is not None:
            cls = getattr(sys.modules[__name__], xmlRoot.get("class"))
            node = cls("")
        else:
            node = GameNode("") 
            
        # _async attribute
        node._setAsync(async)           
        
        nodePath = node.toNodePath()
        nodePath.setName(xmlRoot.tag)          
        optionDefs = self.createOptionDefs(xmlRoot, nodePath)              
        node.refresh(optionDefs)                
        if  xmlRoot.get("type") == "scene":                
            GameNode.notify.debug("make scene")
            
        return node
        
    
    def createOptionDefs(self, xmlRoot, parentNode):        
        """
        Prepare well-formed properties list before applying to GameNode.        
        Return:
            List - list of well-formed properties from xmlRoot
        """
        GameNode.notify.debug("GameNodeBuilder.createOptionDefs")
        optionDefs = []
        for child in xmlRoot.getchildren():            
            if child.get("type") == "function":
                # get function name
                prefix = child.get("prefix", "")
                tag = child.tag
                functionName = prefix + tag
                
                # callable object if need
                obj = child.get("var")
                
                # datatype checking for element text
                datatype = child.get("datatype", "")
                if datatype == "path":
                    from panda3d.core import Filename
                    workingDir = os.getcwd()        
                    workingDir = Filename.fromOsSpecific(workingDir).getFullpath()
                    # create path for panda3d file system
                    params = [workingDir + child.text]
                elif datatype == "list float":
                    paramString = child.text.split(",")
                    params = caster.stringListToListFloat(paramString, 0)
                else:
                    params = [child.text]
                               
                optionDefs.append([params, functionName, obj])
            elif child.get("type") == "node":
                builder = self
                if child.get("builder"):
                    print "system modules: ", sys.modules[__name__]
                    builder = getattr(sys.modules[__name__], child.get("builder"))()
                node = builder.make(child, parentNode.getPythonTag("node")._async)
                node.toNodePath().reparentTo(parentNode)
        return optionDefs
    
    

        
            
            
        