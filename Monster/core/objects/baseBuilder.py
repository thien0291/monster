#import from python
import sys, os
import copy
import json
#import from Panda3d
from panda3d.core import *
from direct.particles.Particles import Particles
from direct.particles.ParticleEffect import ParticleEffect
from direct.particles.ForceGroup import ForceGroup
from direct.gui.DirectGui import *
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.DirectObject import DirectObject
from panda3d.bullet import *
from direct.actor.Actor import Actor
# from Monster.core.modules.managers.MapManager import nodeRoot
from Monster.utility.guihelper import getGeom
# from Monster.extend.objects.shadow import SingleShadow, ShadowFactory, ShadowBase, ShadowBuilder
from Monster.core.objects.input import Panda3DInput
from Monster.extend.objects.particle import ParticleBase
from Monster.utility import caster
from Monster.core.modules.managers.gui import GuiManager


class BaseBuilder(object):
    """
    @summary: BaseBuilder is a base class (virtual) that help factory can make a object from Data input
    """
    def __init__(self, *args, **kwargs):
        """
        @summary: BaseBuilder is a base class (virtual) that help factory can make a object from Data input
        @param kwargs: All data input will add into self.data 
        """
        self.data = kwargs
        
    def buildGameObject(self, world=None):
        """
        @summary: virtual function. It will build a game object
        @param world: default is None
        """
        pass
    
    def buildType(self):
        """
        @summary: virtual function. it will build a prototype (prototype will clone, specify more info and become game object)
        """
        pass
    
    def asynchronize(self, *nodePaths):
        """
        @summary: virtual function - Not know
        @param world: default is None
        """
        pass
    
    def processAttributes(self, data, gameObject):
        """
        @summary: set all attribute to game object
        @param data: (dict) Game Object Data (It must contain "Attribute" key and value as a dict)
        @param gameObject: (GameObject) which game object you want to set attribute 
        """
        if data.has_key("Attribute"):
            attributes_data = data['Attribute']
            gameObject.appendAttribute(attributes_data)
                
    
    def resetTransform(self, nodePath):
        """
        @summary: reset all value to be standard state (Pos, Hpr, Scale)
        @param nodePath: (nodePath) which nodepath will be reset. 
        """
        nodePath.setPos(0, 0, 0)
        nodePath.setHpr(0, 0, 0)
        nodePath.setScale(1)
        
    def createWheel(self, data, obj, nodeRoot):
        vehicle = obj.vehicle
        i = 0
        if data.has_key('Wheels'):
            for wheel_data in data['Wheels'].values():                
                wheel = vehicle.createWheel()
                wheelNP = loader.loadModel(wheel_data['Model'])
                wheelNP.reparentTo(nodeRoot) 
                if not hasattr(obj, 'wheels'):
                    obj.wheels = []
                obj.wheels.append(wheelNP)               
                pos = caster.stringListToListFloat(wheel_data['Pos'].split(','))                
                wheel.setNode(wheelNP.node())
                wheel.setChassisConnectionPointCs(Point3(*pos))
                front = eval(wheel_data['Front'])
                wheel.setFrontWheel(front)
                if front == True:
                    obj.steersIdx.append(i)
                else:
                    obj.forcesIdx.append(i)
                wheel.setWheelDirectionCs(Vec3(0, 0, -1))
                wheel.setWheelAxleCs(Vec3(1, 0, 0))
                wheel.setWheelRadius(0.25)
                wheel.setMaxSuspensionTravelCm(40.0)
            
                wheel.setSuspensionStiffness(40.0)
                wheel.setWheelsDampingRelaxation(2.3)
                wheel.setWheelsDampingCompression(4.4)
                wheel.setFrictionSlip(100.0);
                wheel.setRollInfluence(0.1)
                i += 1
    
    def processBulletData(self, prototypeData, gameObject, nodeRoot, bulletWorld):
        # Bullet physical checking
        if prototypeData.has_key('PhysicBody') and bulletWorld is not None:            
            physicNode = self.createBulletBody(prototypeData['PhysicBody'],
                                                gameObject.nodePath, bulletWorld)
            isTerrain = False
            if isinstance(physicNode, tuple):
                gameObject.vehicle = physicNode[1]
                physicNode = physicNode[0]
                gameObject.physicNode = NodePath(physicNode)
                self.createWheel(prototypeData['PhysicBody'],
                                 gameObject, nodeRoot)
            else:
                gameObject.physicNode = NodePath(physicNode)
            if isinstance(physicNode, BulletCharacterControllerNode):
                gameObject.physicNode.setPos(gameObject.nodePath.getPos())
                gameObject.physicNode.setHpr(gameObject.nodePath.getHpr())
                gameObject.physicNode.setScale(gameObject.nodePath.getScale())
                self.resetTransform(gameObject.nodePath)                
            elif not isinstance(gameObject.physicNode.node().getShape(0), BulletHeightfieldShape): 
                gameObject.physicNode.setPos(gameObject.nodePath.getPos())
                gameObject.physicNode.setHpr(gameObject.nodePath.getHpr())
                gameObject.physicNode.setScale(gameObject.nodePath.getScale())
                self.resetTransform(gameObject.nodePath)
            else:
                isTerrain = True
                
             
            # Default pos, hpr, scale            
            if prototypeData['PhysicBody'].has_key('Pos'):
                pos = prototypeData['PhysicBody']['Pos'].split(',')
                move = VBase3(*caster.stringListToListFloat(pos))
                gameObject.nodePath.setPos(gameObject.nodePath.getPos() + move)                        
            gameObject.nodePath.reparentTo(gameObject.physicNode)  
            
            # Restore height for terrain
            if isTerrain:
                height = gameObject.nodePath.getPythonTag('height')
                gameObject.physicNode.setZ(height / 2)
               
            # Check mask
            if prototypeData['PhysicBody'].has_key('BitMask'):
                masks_data = prototypeData['PhysicBody']['BitMask']
                masks_number = caster.stringListToListInt(masks_data.split(','))
                mask = BitMask32()
                for number in masks_number:
                    mask.setBit(number)
                gameObject.physicNode.setCollideMask(mask)
            else:
                gameObject.physicNode.setCollideMask(BitMask32.allOn())
                      
            self.attachPhysicNode(gameObject, bulletWorld)
            gameObject.realNP = gameObject.nodePath
            gameObject.nodePath = gameObject.physicNode
        
    def attachPhysicNode(self, gameObject, world):
        physicNode = gameObject.physicNode        
        if isinstance(physicNode.node(), BulletRigidBodyNode):
            world.attachRigidBody(physicNode.node())
        elif isinstance(physicNode.node(), BulletGhostNode):
            world.attachGhost(physicNode.node())
        elif isinstance(physicNode.node(), BulletSoftBodyNode):
            world.attachSoftBody(physicNode.node())
        elif isinstance(physicNode.node(), BulletCharacterControllerNode):
            world.attachCharacter(physicNode.node())
        if hasattr(gameObject, 'vehicle') and gameObject.vehicle:
            world.attachVehicle(gameObject.vehicle)
         
    
    def createSingleBulletShape(self, data, nodePath):               
        if data.has_key('HeightFieldImage'):
            heightFieldImg = data['HeightFieldImage'] 
        if data.has_key('Distance'):
            distance = caster.stringToFloat(data['Distance'], 0)
        if data.has_key('Normal'):
            normal = caster.stringListToListFloat(data['Normal'].split(','), 1)
        if data.has_key('Dynamic'):
            isDynamic = False
        if data.has_key('Radius'):
            radius = caster.stringToFloat(data['Radius'], 0)
        if data.has_key('Height'):
            height = caster.stringToFloat(data['Height'], 0)      
        if data.has_key('HalfVector'):
            halfVec = caster.stringListToListFloat(data['HalfVector'].split(','), 1)
        points = []
        isGeom = False
        triangles = []
        if data.has_key('Geom'):
            isGeom = True
        elif data.has_key('Points'):            
            for p in data['Points'].values():
                point = caster.stringListToListFloat(p.split(','), 0)
                points.append(point)
        elif data.has_key("Triangles"):
            for triangleData in data['Triangles'].values():
                triangle = []
                for p in triangleData.values():
                    point = caster.stringListToListFloat(p.split(','), 0)
                    triangle.append(point)
                triangles.append(triangle)
        if data.has_key('Pos'):
            pos = caster.stringListToListFloat(data['Pos'].split(','), 0)
        else:
            pos = [0, 0, 0]           
        shapeType = data['Type']        
        if "sphereshape" in shapeType.lower():
            shape = BulletSphereShape(radius)
        elif "planeshape" in shapeType.lower():
            shape = BulletPlaneShape(Vec3(*normal), distance)
        elif "boxshape" in shapeType.lower():                
            shape = BulletBoxShape(Vec3(*halfVec))
        elif "cylindershape" in shapeType.lower():
            shape = BulletCylinderShape(radius, height, ZUp)
        elif "capsuleshape" in shapeType.lower():                
            shape = BulletCapsuleShape(radius, height, ZUp)
        elif "coneshape" in shapeType.lower():
            shape = BulletCapsuleShape(radius, height, ZUp)
        elif "convexhull" in shapeType.lower():
            shape = BulletConvexHullShape()
            if isGeom == True:
                geomNodes = nodePath.findAllMatches('**/+GeomNode')
                geomNode = geomNodes.getPath(0).node()
                geom = geomNode.getGeom(0)
                shape.addGeom(geom)
            elif len(points) > 0:
                for p in points:
                    shape.addPoint(p)
        elif 'trianglemesh' in shapeType.lower():
            mesh = BulletTriangleMesh()
            if isGeom == True:
                geomNodes = nodePath.findAllMatches('**/+GeomNode')                
                for idx in xrange(0, len(geomNodes)):                    
                    geomNode = geomNodes.getPath(idx).node()                
                    for geom in geomNode.getGeoms():                    
                        mesh.addGeom(geom)
            elif len(triangles) > 0:
                for triangle in triangles:
                    mesh.addTriangle(*triangle)                    
            shape = BulletTriangleMeshShape(mesh, dynamic=isDynamic)
        elif "heightfield" in shapeType.lower():    
            img = PNMImage(heightFieldImg)      
            img.makeGrayscale()
            shape = BulletHeightfieldShape(img, height, ZUp)  
            shape.setUseDiamondSubdivision(True)      
            offset = img.getXSize() / 2.0 - 0.5            
            nodePath.setPos(-offset, -offset, -height / 2.0)
            nodePath.setPythonTag('height', height)
        return shape, pos
    
    def createBulletBody(self, data, nodePath, world):
        shapes = {}
        for key, value in data['Shapes'].items():
            shape, pos = self.createSingleBulletShape(value, nodePath) 
            shapes[key] = { 'shape' : shape, 'pos' : pos}
        # Create basic body node
        name = nodePath.getName() + data['Name']
        nodeType = data['Type']
        if "rigid" in nodeType.lower() or "vehicle" in nodeType.lower():
            node = BulletRigidBodyNode(name)   
            for shape in shapes.values():
                if isinstance(shape['shape'], BulletTriangleMeshShape) or\
                    isinstance(shape['shape'], BulletHeightfieldShape):                    
                    node.addShape(shape['shape'])
                else:
                    pos = Vec3(*shape['pos'])
                    node.addShape(shape['shape'], TransformState.makePos(pos))
            if data.has_key('Mass'):                
                node.setMass(caster.stringToFloat(data['Mass'], 1))         
        elif "ghost" in nodeType.lower():
            node = BulletGhostNode(name)     
            for shape in shapes.values():
                if isinstance(shape['shape'], BulletTriangleMeshShape) or\
                    isinstance(shape['shape'], BulletHeightfieldShape):                     
                    node.addShape(shape['shape'])
                else:                    
                    pos = Vec3(*shape['pos'])
                    node.addShape(shape['shape'], TransformState.makePos(pos))
        # Create extend body node
        elif "character" in nodeType.lower():            
            stepHeight = caster.stringToFloat(data['StepHeight'], 0.4)
            node = BulletCharacterControllerNode(shapes.values()[0]['shape'], stepHeight, name)
            
        if "vehicle" in nodeType.lower():            
            node.setDeactivationEnabled(False)
            vehicle = BulletVehicle(world, node)            
            vehicle.setCoordinateSystem(ZUp)            
            return node, vehicle     
        return node
    
    def processMaterialInfo(self, data, nodePath):
        if data.has_key("Material") and len(data['Material']) > 0:
            myMaterial = Material()
            material = data['Material']
            if material.has_key('Ambient'):
                ambient = caster.stringListToListFloat(material['Ambient'].split(','))
                myMaterial.setAmbient(VBase4(*ambient))
            if material.has_key('Diffuse'):
                diffuse = caster.stringListToListFloat(material['Diffuse'].split(','))
                myMaterial.setDiffuse(VBase4(*diffuse))
            if material.has_key('Emission'):
                emission = caster.stringListToListFloat(material['Emission'].split(','))
                myMaterial.setEmission(VBase4(*emission))
            if material.has_key('Shininess'):
                shininess = caster.stringToFloat(material['Shininess'], 2.0)
                myMaterial.setShininess(shininess)
            if material.has_key('Specular'):
                specular = caster.stringListToListFloat(material['Specular'].split(','))
                myMaterial.setSpecular(VBase4(*specular))            
            nodePath.setMaterial(myMaterial)
            
    def processTags(self, data, gameObject):
        if data.has_key("Tags"):            
            tags = data['Tags'].split(",")
            tags = [x.strip() for x in tags]
            gameObject.appendTags(*tags)
            
    def processScripts(self, data, gameObject):        
        if data.has_key("Scripts"):                      
            scriptFiles = data['Scripts'].split(",")
            scriptFiles = [x.strip() for x in scriptFiles]
            for scriptPath in scriptFiles:
                PROJECT_FOLDER = os.getcwd()            
                scriptFilePath = os.path.join(PROJECT_FOLDER, scriptPath)
                name = os.path.basename(scriptFilePath)
                name, ext = os.path.splitext(name) 
                SCRIPT_FOLDER = os.path.dirname(scriptFilePath)            
                if sys.path.count(SCRIPT_FOLDER) == 0:                
                    sys.path.append(SCRIPT_FOLDER)            
                scriptFile = [name]
                modules = map(__import__, scriptFile)
                for module in modules:
                    # module.owner = model
                    # #print "MODEL > GETPOS", model.getPos()
                    script = module.Scripting(gameObject)
                    # taskMgr.add(script.run, gameObject.nodeName + scriptFilePath + 'script', uponDeath=script.cleanUp)
                    gameObject.appendScripts(script)
        
    