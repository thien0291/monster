from panda3d.core import WindowProperties
hidden = False

# Function Utility for Mouse
def getMousePos():
    if base.mouseWatcherNode.hasMouse():
        x = base.mouseWatcherNode.getMouseX()
        y = base.mouseWatcherNode.getMouseY()
        return x, y
    
def setMousePos(x, y):
    if base.mouseWatcherNode.hasMouse():
        base.win.movePointer(0, x, y)
        
def getCenter():
    props = base.win.getProperties()
    return props.getXSize()/2, props.getYSize()/2

def getRatio():
    x, y = getCenter()
    return x / y
        
def hideMouse():
    global hidden
    props = WindowProperties(base.win.getProperties())
    props.setCursorHidden(True) 
    base.win.requestProperties(props)
    hidden = True
    
def isShow():
    global hidden
    return not hidden
    
def showMouse():
    global hidden
    props = WindowProperties(base.win.getProperties())
    props.setCursorHidden(False) 
    base.win.requestProperties(props)
    hidden = False
    
def setCursorFile(filePath):
    global hidden
    props = WindowProperties(base.win.getProperties())
    props.setCursorFilename(filePath)
    base.win.requestProperties(props)
    