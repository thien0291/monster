def stringToFloat(inStr, default):
    try:
        return float(inStr)
    except:
        return default
    
def stringToInt(inStr, default):
    try:
        return int(inStr)
    except:
        return default
    
def stringToBool(inStr, default):
    try:
        return bool(inStr)
    except:
        return default
    
def stringListToListFloat(inList, default=0):
    lst = []
    for val in inList:        
        lst.append(stringToFloat(val, default))    
    return lst

def stringListToListInt(inList, default=0):
    lst = []
    for val in inList:        
        lst.append(stringToInt(val, default))    
    return lst

def listNumberToString(inList):
    retsult = ""
    for item in inList:
        retsult = retsult + str(item) + ","
    retsult = retsult[0:-1]
    return retsult

def isFloat(inStr):
    try:
        float(inStr)
        return True
    except:
        return False
    
