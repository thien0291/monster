import random
import string

def randomString(n):
	return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(n))


print randomString(5)
print randomString(5)