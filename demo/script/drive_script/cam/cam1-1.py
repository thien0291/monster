from panda3d.core import Vec3
from direct.showbase.PythonUtil import lerp


class Scripting():
    def __init__(self, model):
        self.camera = model
        self.direction = -1
        self.back = -10
        self.height = 2
        self.lookHeight = 2
        self.camera.appendInitFunc(self.init)


    def init(self, task):
        #forward vector not meaning forward. It's define where camera position vs self.target
        self.target = None        

        return task.done
        
    
    def run(self, task):       
        self.target = store.car
        self.vecForward = base.render.getRelativeVector(self.target.physicNode, (0,self.direction,0))                        
        self.vecForward *= -self.back
        self.vecForward += Vec3(0, 0, self.height)
        prevPos = self.camera.getPos()        
        pos = self.target.getPos() + self.vecForward * 1.3
        p = lerp(prevPos, pos, 0.07)
        self.camera.setPos(pos)
        
        
        lookPos = self.target.getPos()
        lookPos.setZ(lookPos.getZ() + self.lookHeight)
        self.camera.lookAt(lookPos)
        return task.cont
    
    def cleanUp(self, task):
        return task.done