mapx = 0
mapy = 0

def map2d_init(task):
    global mapx, mapy
    
    road = screen.current.getObject('Road')
    minPoint, maxPoint = road.realNP.getTightBounds()  
    mapx = max( abs(minPoint.getX()) ,  abs(maxPoint.getX()) )
    mapy = max( abs(minPoint.getY()) ,  abs(maxPoint.getY()) )
   
    return task.done

def map2d_run(task):
    global mapx, mapy
    car = screen.current.getObject('RaceCar')
    carx = car.getX()
    cary = car.getY()
    x_scale = (carx / mapx ) 
    y_scale = (cary / mapy ) 
        
    gui = screen.current.gui
    car_point = gui.getGui("car_point")
    car_point.setPos(x_scale, 0, y_scale)
    
    return task.cont