from panda3d.core import BitMask32

class Scripting():
    def __init__(self, model):
        self.model = model        
        self.model.appendFuncs(self.init)
        
    def init(self, task): 
        self.model.hide()           
        return task.done    
    
    def run(self, task):
        return task.cont
    
    def cleanUp(self, task):
        return task.done