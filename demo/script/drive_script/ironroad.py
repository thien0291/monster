from panda3d.core import Texture

class Scripting():
    def __init__(self, model):
        self.model = model
        self.model.appendFuncs(self.init)
        
    def init(self, task):    
        roadTexture = self.model.findTexture('RoadTexture')        
        roadTexture.setMagfilter(Texture.FTDefault)
        roadTexture.setMinfilter(Texture.FTDefault)     
        grassTexture = self.model.findTexture('Grass0139_22_M')
        grassTexture.setMagfilter(Texture.FTLinearMipmapLinear)
        grassTexture.setMinfilter(Texture.FTLinearMipmapLinear)       
        
        return task.done    
    
    def run(self, task):
        return task.cont
    
    def cleanUp(self, task):
        return task.done