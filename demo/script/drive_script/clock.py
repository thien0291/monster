
def clock_init(task):
    store.clock = RaceClock()
    store.clock.start()
    return task.done

def clock_run(task):
    store.clock.run()
    second = store.clock.getSecond()
    minute = store.clock.getMinute()
    time_str = "%d:%02d" % (minute, second)
    gui = screen.current.gui
    clock_time = gui.getGui("clock-time")
    if clock_time['text'] != time_str:
        clock_time['text'] = time_str
    return task.cont

class RaceClock():
    def __init__(self, start=0):
        self.total_second = start        
        self.isStart = False
        
    def start(self):
        self.isStart = True
        
    def pause(self):
        self.isStart = False
        
    def stop(self):
        self.isStart = False
        self.total_second = 0
        
    def run(self):
        if self.isStart:
            dt = globalClock.getDt()
            self.total_second += dt
        
        
    def getSecond(self):
        return int(self.total_second % 60)
    
    def getMinute(self):
        return int(self.total_second / 60)