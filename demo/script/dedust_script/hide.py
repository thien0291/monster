
class Scripting():    
    def __init__(self, model):  
        self.model = model        
        self.model.nodePath.hide()
        self.model.realNP.hide()
        self.model.appendInitFunc(self.init)
        
    def init(self, task):
        return task.done
    
    def run(self, task):
        # Code here          
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        return task.done