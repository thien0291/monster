from Monster.core.objects.map import Map
from Monster.core.objects.message import GameMessage

def buttonClick(buttonName, postBackToTagList):
    print "Button Click Detected"
    postBackToTagTuple = tuple(postBackToTagList)
    print "postBackToTupe", postBackToTagTuple
    message = GameMessage((buttonName,), postBackToTagTuple, {'event': "click"})
    Map.current.dispatch(message)
    print "Message Click Done"