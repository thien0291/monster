from panda3d.core import loadPrcFileData
from panda3d.core import ConfigVariableString
from config import *
from Monster.core.modules.managers.MapManager import MapManager
from Monster.utility import instruction

# Panda3d Initialize
import direct.directbase.DirectStart

# Show fps
base.setFrameRateMeter(True)

# Monster Initialize
class Monster():
    def __init__(self):        
        self.mm = MapManager()
        self.mm.loadMapListFromFile('map/mapList.xml')        

# Monster run

g = Monster()
run()