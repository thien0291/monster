//Cg

void vshader(float4 vtx_position : POSITION,
             float2 vtx_texcoord0: TEXCOORD0,
             float3 vtx_normal: NORMAL,

             uniform float4x4 trans_model_to_clip_of_light,
             uniform float4x4 mat_modelproj,
             uniform float4x4 mat_modelview,
             uniform float3 k_scale,
             uniform float4 k_push,
             out float4 l_position : POSITION,
             out float2 l_texcoord0 : TEXCOORD0,
             out float4 l_shadowcoord : TEXCOORD1,
             out float3 l_normal : TEXCOORD2,
             out float l_distance : TEXCOORD3
             )

{
	float4 position = vtx_position * float4(k_scale, 1);
	
	// vertex position
	l_position = mul(mat_modelproj, vtx_position);
	
	l_distance = length( mul( mat_modelview, vtx_position ));
	// Pass through texture coordinate for main texture.
	l_texcoord0 = vtx_texcoord0;
	
	l_normal.xyz = vtx_normal.xyz;

	//------------Calculate shadow coordinate---------------------------------------------------------------------------------
	// Calculate light-space clip position.
	float4 pushed = vtx_position + float4(vtx_normal * k_push, 0);
	float4 l_lightclip = mul(trans_model_to_clip_of_light, pushed);
	
	// Calculate shadow-map texture coordinates.
	l_shadowcoord = l_lightclip * float4(0.5,0.5,0.5,1.0) + l_lightclip.w * float4(0.5,0.5,0.5,0.0);
}


void fshader(in float2 l_texcoord0 : TEXCOORD0,
             in float4 l_shadowcoord : TEXCOORD1,
             in float3 l_normal : TEXCOORD2,
             in float l_distance: TEXCOORD3,
          	 in uniform float4 alight_ambient0,	
          	 in uniform float4x4 dlight_direct0_rel_world,	      	  
	      	 in uniform float4x4 attr_material,
	      	 in uniform float4 attr_fog,
	      	 in uniform float4 attr_fogcolor,
	      	 in uniform float k_fog_mode,
             uniform sampler2D tex_0 : TEXUNIT0,
             uniform sampler2D k_Ldepthmap : TEXUNIT1,             
             out float4 o_color:COLOR)
{

//------------------Calculate the surface lighting factor-------------------------------------------------------------------
  float4 lightColor = dlight_direct0_rel_world[0];
  float4 lightSpecularColor= dlight_direct0_rel_world[1];
  float3 lightDirection = dlight_direct0_rel_world[2];
  float4 lightHalf = dlight_direct0_rel_world[3];
  
  // Object material
  float4 m_ambient = attr_material[0];
  float4 m_diffuse = attr_material[1];
  float4 m_emission = attr_material[2];
  float4 m_specular = attr_material[3];
  float4 m_shininess = attr_material[3].w;

  // lighting emission
  float4 l_emission = m_emission;
  
  // lighting ambient
  float4 l_ambient = alight_ambient0 * m_ambient;

  // lighting diffuse
  float3 N = normalize( l_normal );
  float3 L = normalize( lightDirection.xyz );
  float diffuseLight = max(dot(N, L), 0);
  float4 l_diffuse = diffuseLight * lightColor * m_diffuse;  

  // lighting specular  
  float3 H = normalize(lightHalf);
  float specularLight = pow(max(dot(N, H), 0), m_shininess);

  if (diffuseLight <= 0) 
	  specularLight = 0;
  float4 l_specular = specularLight * m_specular * lightSpecularColor;
  
  //float3 circleoffs = float3(l_lightclip.xy / l_lightclip.w, 0);
  //float falloff = saturate(1.0 - dot(circleoffs, circleoffs));
  
  float4 baseColor = saturate(tex2D(tex_0, l_texcoord0));
  float shade = tex2Dproj(k_Ldepthmap,l_shadowcoord);
  //o_color = baseColor * ( shade * l_diffuse  + l_specular + l_ambient + l_emission);
 
  //------------------Calculate the fog-------------------------------------------------------------------
  float density = attr_fog.x;
  float start = attr_fog.y;
  float end = attr_fog.z;
  float fog_scale = attr_fog.w;  
  float fogFactor;
  
  if (k_fog_mode > 0)
  {
	  if (k_fog_mode == 0) // linear fog
	  {
	  	fogFactor = saturate((end - l_distance) / (end - start));
	  }
	  else if (k_fog_mode == 1) // exp fog
	  {  
	    float fogExponent  = l_distance * density;  
	    fogFactor = exp2(-abs(fogExponent));
	  }
	  else if (k_fog_mode == 2) // exp2 fog
	  {
	    float fogExponent  = pow(l_distance * density, 2); 
	    fogFactor = exp2(-abs(fogExponent));
	  }  
	  
  	  o_color.xyz = lerp(attr_fogcolor.xyz, o_color.xyz, fogFactor);
  }
  o_color.a = 1;
  o_color = float4(1, 1, 1, 1);
}
