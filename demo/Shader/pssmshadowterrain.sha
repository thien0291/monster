//Cg
//Cg profile arbvp1 arbfp1

void vshader( in float4 vtx_position : POSITION,
	      in float3 vtx_normal : NORMAL,
              in float2 vtx_texcoord0 : TEXCOORD0,
              in float2 vtx_texcoord3 : TEXCOORD3,
              in uniform float4x4 mat_modelproj,
              uniform float4x4 trans_model_to_clip_of_cam0,
             uniform float4x4 trans_model_to_clip_of_cam1,
             uniform float4x4 trans_model_to_clip_of_cam2,
              in uniform float4x4 mat_modelview,      
              uniform float4x4 trans_model_to_maincam,
	      in uniform float4 k_tscale,	      
	      in uniform float4 k_push,
	      in uniform float4 k_scale,	      
	      	  out float4 l_cpos,
              out float2 l_texcoord0 : TEXCOORD0,
              out float2 l_texcoord3 : TEXCOORD3,
              out float3 l_normal : TEXCOORD2,
              out float4 l_shadowcoord0 : TEXCOORD4,
              out float4 l_shadowcoord1 : TEXCOORD5,
              out float4 l_shadowcoord2 : TEXCOORD6,              
              out float l_distance : TEXCOORD1,
              out float4 l_position : POSITION)
{  
  float4 position = vtx_position * k_scale;  
  l_position=mul(mat_modelproj,vtx_position);
  l_texcoord0=vtx_texcoord0*k_tscale;
  l_texcoord3=vtx_texcoord3;
  vtx_normal.z /= k_scale.z;
  vtx_normal.x *= -1;
  l_normal.xyz = vtx_normal.xyz;
  float4 vertexViewPos = mul( mat_modelview, vtx_position );
  l_distance = length( vertexViewPos );
  l_cpos= mul( trans_model_to_maincam, vtx_position);
  //------------Calculate shadow coordinate---------------------------------------------------------------------------------
  float4 pushed = vtx_position + float4(vtx_normal * k_push, 0);
  float4 l_lightclip = mul(trans_model_to_clip_of_cam0, pushed);

  // Calculate shadow-map texture coordinates.
	l_shadowcoord0 = l_lightclip * float4(0.5,0.5,0.5,1.0) + l_lightclip.w * float4(0.5,0.5,0.5,0.0);
	
	// Calculate cam1-space clip position.
	l_lightclip = mul(trans_model_to_clip_of_cam1, pushed);
	pushed = vtx_position + float4(vtx_normal * k_push * 4, 0);
	// Calculate shadow-map texture coordinates.
	l_shadowcoord1 = l_lightclip * float4(0.5,0.5,0.5,1.0) + l_lightclip.w * float4(0.5,0.5,0.5,0.0);
	
	// Calculate cam2-space clip position.
	pushed = vtx_position + float4(vtx_normal * k_push * 10, 0);
	l_lightclip = mul(trans_model_to_clip_of_cam2, pushed);
	
	// Calculate shadow-map texture coordinates.
	l_shadowcoord2 = l_lightclip * float4(0.5,0.5,0.5,1.0) + l_lightclip.w * float4(0.5,0.5,0.5,0.0);

}

void fshader( in float4 l_position : POSITION,
              in float2 l_texcoord0 : TEXCOORD0,
              in float2 l_texcoord3 : TEXCOORD3,
              in float3 l_normal : TEXCOORD2,
              in float4 l_shadowcoord0 : TEXCOORD4,
              in float4 l_shadowcoord1 : TEXCOORD5,
              in float4 l_shadowcoord2 : TEXCOORD6,
              in float l_distance: TEXCOORD1,
          	  in uniform float4 alight_ambient0,	
          	  in uniform float4x4 dlight_direct0_rel_world,	      	  
	      	  in uniform float4x4 attr_material,	      	  
	      	  in uniform float4 attr_fog,
	          in uniform float4 attr_fogcolor,
	       	  in uniform float k_fog_mode,
	       	  in uniform float3 k_LdepthRange,
	       	  in float4 l_cpos,
              in uniform sampler2D tex_0 : TEXUNIT0,
              in uniform sampler2D tex_1 : TEXUNIT1,
              in uniform sampler2D tex_2 : TEXUNIT2,
              in uniform sampler2D tex_3 : TEXUNIT3,
              in uniform sampler2D tex_4 : TEXUNIT4,
              in uniform sampler2D tex_5 : TEXUNIT5,
              uniform sampler2D k_Ldepthmap0 : TEXUNIT6,             
              uniform sampler2D k_Ldepthmap1 : TEXUNIT7,
              uniform sampler2D k_Ldepthmap2 : TEXUNIT8, 
              out float4 o_color : COLOR )
{

//------------------Calculate the surface lighting factor-------------------------------------------------------------------
  float4 lightColor = dlight_direct0_rel_world[0];
  float4 lightSpecularColor= dlight_direct0_rel_world[1];
  float3 lightDirection = dlight_direct0_rel_world[2];
  float4 lightHalf = dlight_direct0_rel_world[3];
  
  // Object material
  float4 m_ambient = attr_material[0];
  float4 m_diffuse = attr_material[1];
  float4 m_emission = attr_material[2];
  float4 m_specular = attr_material[3];
  float4 m_shininess = attr_material[3].w;

  // lighting emission
  float4 l_emission = m_emission;
  
  // lighting ambient
  float4 l_ambient = alight_ambient0 * m_ambient;

  // lighting diffuse
  float3 N = normalize( l_normal );
  float3 L = normalize( lightDirection.xyz );
  float diffuseLight = max(dot(N, L), 0);
  float4 l_diffuse = diffuseLight * lightColor * m_diffuse;  

  // lighting specular  
  float3 H = normalize(lightHalf);
  float specularLight = pow(max(dot(N, -H), 0), m_shininess);

  float4 l_specular = specularLight * m_specular * lightSpecularColor;

  // alpha splatting and lighting
  float4 tex1=tex2D(tex_0,l_texcoord0);
  float4 tex2=tex2D(tex_1,l_texcoord0);
  float4 tex3=tex2D(tex_2,l_texcoord0);
  float alpha1=tex2D(tex_3,l_texcoord3).z;
  float alpha2=tex2D(tex_4,l_texcoord3).z;
  float alpha3=tex2D(tex_5,l_texcoord3).z;
  o_color =tex1*alpha1;
  o_color+=tex2*alpha2;
  o_color+=tex3*alpha3;
  
  float mapval = 1;
  float shade = 1;
  float4 proj = 1;
  
  // calculate shade
  if (l_cpos.y <= k_LdepthRange.x)
  {
  	proj = l_shadowcoord0 / l_shadowcoord0.w;
  	mapval = tex2Dproj(k_Ldepthmap0, l_shadowcoord0);
  }
  else 
  {
  	if (l_cpos.y <= k_LdepthRange.y)
  	{
  		proj = l_shadowcoord1 / l_shadowcoord1.w;
  		mapval = tex2Dproj(k_Ldepthmap1, l_shadowcoord1);
  	}
  	else
  	{
  		if (l_cpos.y <= k_LdepthRange.z)
  		{
  			proj = l_shadowcoord2 / l_shadowcoord2.w;
  			mapval = tex2Dproj(k_Ldepthmap2, l_shadowcoord2);
  		}
  	}
  }
  shade = (mapval > proj.z);
  
  o_color=o_color*(l_diffuse * shade + l_specular + l_ambient + l_emission);
  //------------------Calculate the fog-------------------------------------------------------------------
  float density = attr_fog.x;
  float start = attr_fog.y;
  float end = attr_fog.z;
  float fog_scale = attr_fog.w;  
  float fogFactor;
  
  if (k_fog_mode > 0)
  {
	  if (k_fog_mode == 0) // linear fog
	  {
	  	fogFactor = saturate((end - l_distance) / (end - start));
	  }
	  else if (k_fog_mode == 1) // exp fog
	  {  
	    float fogExponent  = l_distance * density;  
	    fogFactor = exp2(-abs(fogExponent));
	  }
	  else if (k_fog_mode == 2) // exp2 fog
	  {
	    float fogExponent  = pow(l_distance * density, 2); 
	    fogFactor = exp2(-abs(fogExponent));
	  }  
	  
  	  o_color.xyz = lerp(attr_fogcolor.xyz, o_color.xyz, fogFactor);
  }
  
  o_color.a=1.0;   
}
