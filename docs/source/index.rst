.. Monster documentation master file, created by
   sphinx-quickstart on Sat Apr 19 16:45:46 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Monster's documentation!
===================================

This project is in development. We will comeback soon!

Contents:

.. toctree::
   :maxdepth: 2

   _templates/globalvars
   _templates/gamenode
   _templates/builder
   _templates/extendmodules
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

