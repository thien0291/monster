.. GameNode 

Monster GameNode
================

(This document is in development)

GameNode is core unit of Monster framework. GameNode class overrides PandaNode of Panda3D engine, it can be
game unit (human, animal, ground, sky, water, light, ...) or non game unit (scene, sound, gui,...). Defining 
GameNodes and their relationship is defining your game.


GameNode can be used in python code or in XML file.

1.In python, it will look like that::

   from Monster.core.objects.gamenode import GameNode
   myBall1 = GameNode("yellow_ball")
   myNodePath = render.attachNewNode(myBall)
   
render is Panda3D universal object, it represents your current scene. See more: http://www.panda3d.org/manual/index.php/Scene_Graph_Manipulations
When GameNode is added to render, it will be handled by panda3d NodePath object (myNodePath). Unlike Panda3D, 
we provide other way to switch between GameNode and NodePath. MonsterFramework provides a shortcut utility 
for that::

   from Monster.utility.shortcut import getGameNode
   myBall2 = getGameNode(myNodePath)
   # We can check myBall2 
   if myBall2 is myBall1 and isinstance(myBall2, GameNode):
      print "It works!"
      
   # GameNode to NodePath
   myBall2.toNodePath()
   
Please notice: we cannot use NodePath.node() to retrieve GameNode object.   


2.In XML file, it will perform as::

   <Node type="node" class="GameNode"> 
      <Name type="function" prefix="set">simple_node</Name>
      <Model type="function" prefix="load" var="loader" datatype="path">/asset/smiley</Model>
      <Pos type="function" prefix="set" datatype="list float">2, 3, 4</Pos>
   </Node>
   
   
With python code, we can define objects and their relationship manually (do the same as Panda3d). Therefore, we
go deep into XML file syntax


.. _gamenode_xml_layout:

XML layout
----------

In python, to create GameNode from XML file, you just::

   from Monster.core.objects.gamenode import GameNode
   mynode = GameNode.create("path_to_layout_file.xml")
   # Check mynode
   if isinstance (mynode, GameNode):
      print "It works!"

We call xml file is "layout" because it can represent how nodes display together. When you define Node tag, 
you just declare attribute type="node" in this tag::
 
   <NodeName type="node"></NodeName>
   <Duck type="node"></Duck>
   
If you want to declare which type of GameNode, you can use class attribute::

   <Cat type="node" class="AnimalNode"></Cat>
   
Be make sure AnimalNode is inheritance of GameNode. You can define AnimalNode your self. Moreover, we support 
some builtin GameNode (WaterNode, SkyNode, TerrainNode,...), they are introduced in this page: (link_to_page)

Next, we explore Node's properties. At this time, Monster only support function property::

   <Aircraft type="node">
      <Pos type="function" prefix="set" datatype="list float">3, 4, 5</Pos>
   </Aircraft>
   
or simple::

   <Aircraft type="node">
      <setPos type="function" datatype="list float">3, 4, 5</setPos>
   </Aircraft>
   
   
   
Node's xml attribute matrix:

=========  =======  ===============================================
attribute  require  description
=========  =======  ===============================================
type       yes      must equals to "node" (type="node")
class      no       type of GameNode
builder    no       specific your builder class
=========  =======  ===============================================
  
  
   
Property's xml Attribute matrix:

=========  =======  ==================================
attribute  require  description
=========  =======  ==================================
type       yes      currently support type="function"
var        no       object call this function
prefix     no       prefix of function name
datatype   no       use when data of tag is not String
=========  =======  ==================================



Property type matrix:

========     =========== 
type         python type
========     ===========
function     function
node         GameNode
========     ===========



Data type matrix:

==========    ===================
datatype      python type
==========    ===================
list float    list of float items
list int      list of int items
float         float
int           int
list          list of string
==========    ===================


XML Node Recursive
------------------

When you look at Property type matrix, you can see "node" type property. It allows you define parent and
child relationship::

   <!-- parent -->
   <Enemies type="node">
      <!-- child -->
      <Aircraft type="node">
         <setPos type="function" datatype="list float">3, 4, 5</setPos>         
         <Gear1 type="node">
            <setPos type="function" datatype="list float">10, 0, 0</setPos>
         </Gear2>         
         <Gear2 type="node">
            <setPos type="function" datatype="list float">-10, 0, 0</setPos>
         </Gear2>         
         <Missile type="node">
            <setPos type="function" datatype="list float">4, 5, 6.4</setPos>         
         </Missle>
      </Aircraft>      
      <!-- child -->
      <Monster type="node">         
      </Monster>
   </Enemies>
   

Background Loading
------------------
(To be defined)

GameNode provides background loading. It supports for the intialization model from egg file. GameNode.create
has a parameter **async** which determines that node will be loaded asynchronously or not(default: False). The branch starts
from this node (and it's children, grand children,...) will have the same effect::
   
   node = GameNode.create("path_to_file.xml", async=True)
   
Or, you just provide **async** as attribute in XML node tag without specify **async** parameter in Python code ::

   <MyBag type="node" async="True" />   


Relate topic: :ref:`background_loading_scene`
   
   
   

   
 



   
   
   
   
   


 
   
 
  
  
  
