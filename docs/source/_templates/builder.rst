.. How Monster Builder Work
   
How Monster Builder Work!
=========================

(This document is in development)

We can found builders in package Monster.core.objects.gamenode


In previous document for GameNode :ref:`gamenode_xml_layout`, we leared about XML definition and how to 
load nodes from XML layout file. This topic will give you more background understanding of "create" function 
and loading flow.
   
Example, we have an input "simple_game_node.xml" file as below::

   <MyBall type="node" class="MyGameNode">
      <setPos type="function" datatype="list float">1, 0, 0.5, 1</setPos>
      <Model type="function" prefix="load" var="loader" datatype="path">/asset/smiley</Model>
   </MyBall>
   
Then we use GameNode.create to load this xml file::

   mynode = GameNode.create("simple_game_node.xml")
   
Well, we have done with using it but what happened in Monster Framework? Let's see!

Data flow when you call GameNode.create::

   XML --(1)--> python list --(2)--> GameNode (overrides PandaNode of Panda3D engine)

(1): lxml - thirdparty library for parsing xml file into list properties

(2): GameNodeBuilder - convert list properties into GameNode

The code of GameNode.create function::

   from Monster.core.objects.gamenode import GameNodeBuilder
   # Step 1
   io = XmlIO("simple_game_node.xml")
   xmlRoot = io.getRoot() # lxml ElementTree
   # Step 2
   builder = GameNodeBuilder()
   node = builder.make(xmlRoot) # GameNode object
   return node
   
   
Step 1::

   io = XmlIO("simple_game_node.xml")
   xmlRoot = io.getRoot() # lxml ElementTree

   
simple_game_node.xml is layout file. It represents nodes and relationship between nodes. Currently, we are 
using thirdparty library lxml for loading xml file. xmlRoot is ElementTree object. 

Step 2::

   builder = GameNodeBuilder()
   node = builder.make(xmlRoot) # GameNode object
   return node
   
After getting lxml ElementTree object, you just passing to make function of GameNodeBuilder. The "make"
function will process lxml Element Tree into GameNode object.

Create Your Own Builder
-----------------------

Function tag is XML tag which has type == "function" 
When builder is processing function tag, it will parse into python list. The list looks
like as below::
   
   [list_params, "functionName", callableObject]
   
We call this list is "function call property - FC property".
(Currently, we only support FC property, I would like to support more type of property, this document will be updated)

Function list Example::

   [[0, 1, 1, 0], "setColor", None]
   
If callableObject is not None, it will call function: callableObject.functionName(*params). Otherwise, 
generated GameNode will call it. It means we can call all GameNode functions by Function tag. To parse
XML tag, we already have GameNodeBuilder.

GameNodeBuilder contains two function for making GameNode: createOptionDefs, make

   . createOptionDefs: create list of FC properties

   . make: create GameNode from lxml ElementTree
   

More over, we can extend GameNode and GameNodeBuilder. Please follow below sample to create your own builder.

Sample extend builder (see more lxml parsing tutorial here: http://lxml.de/parsing.html)::

   from Monster.core.objects.gamenode import GameNodeBuilder
   from Monster.utility import caster
   
   class MyBuilder(GameNodeBuilder):
   
      def make(self, xmlRoot):
         node = GameNodeBuilder.make(self, xmlRoot) # panda3d NodePath
         # write your stuff 
         
         # end stuff
         return node
         
      def createOptionDefs(self, elementTree, parentNode):
         """
         This function is called for each XML tag has type="node"
         """
         optionDefs - GameNodeBuilder.createOptionDefs(self, elementTree, parentNode)
         
         # write your stuff 
         # parsing lxml ElementTree
         for child in elementTree.getchildren():
            if child.get("type") == "function":
               if child.get("color") == "true":
                  # define your property attribute (ex: color)    
                  paramString = child.text.split(",")
                  params = caster.stringListToListFloat(paramString, 0)  
                  # create FC property
                  optionDef = [params, "setColor", None]
                  optionDefs.append(optionDef)
         # end stuff
               
         return optionDefs
         
Simple GameNode::

   from Monster.core.objects.gamenode import GameNode
   
   class MyGameNode(GameNode):
   
      def setColor(r, g, b, a):
         print "set color", r, g, b, a
         self.r = r
         
      def getR():
         return self.r
            
         
Well, you have done the extend works. Using it now!
         
your xml layout file "my_ball.xml" can use attribute "color" for FC property tag. Because we didn't parse 
tag name in your builder, so it can be everything::
   
   <Ball type="node" class="MyGameNode" builder="MyBuilder">
      <abcdef type="function" color="true">0.9654, 0, 0.5, 1</abcdef>
   </Ball>
   
then you just load your xml file in python::

   gameNode = GameNode.create("my_ball.xml") # GameNode
   # To test your works
   if isinstance(gameNode, MyGameNode) and gameNode.getR() == 0.9654:
      print "It works!"
      
 
  
   

                
         
   
  


