'''
Created on Apr 20, 2014

@author: thinhtran
'''
import unittest
from Monster.core.objects.gamenode import GameNodeBuilder, SceneNode
from Monster.utility.io import XmlIO
from Monster.core.modules.shortcut import getGameNode

class Test(unittest.TestCase):
    def setUp(self):
        from panda3d.core import loadPrcFileData 
        loadPrcFileData('', 'window-type none')        
        import direct.directbase.DirectStart


    def tearDown(self):
        pass


    def test_get_game_node(self):        
        io = XmlIO("asset/simple_3d_scene.xml")
        xmlRoot = io.getRoot()
        builder = GameNodeBuilder()
        node = builder.make(xmlRoot)        
        isNode = isinstance(node, SceneNode)
        self.assertEqual(isNode, True)
        
   
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    