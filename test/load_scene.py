'''
Created on Apr 20, 2014

@author: thinhtran
'''
import unittest
from panda3d.core import loadPrcFileData, NodePath 
loadPrcFileData('', 'window-type none')
loadPrcFileData('', 'notify-level-Monster debug')
loadPrcFileData('', 'notify-level-Monster.core.objects.gamenode.GameNode debug')


from Monster.core.objects.gamenode import GameNodeBuilder, GameNode
from Monster.utility.io import XmlIO

class Test(unittest.TestCase):
    def setUp(self):
        import direct.directbase.DirectStart


    def tearDown(self):
        pass

    def test_load_scene_node_xml(self):        
        io = XmlIO("asset/simple_3d_scene.xml")
        xmlRoot = io.getRoot()
        builder = GameNodeBuilder()
        node = builder.make(xmlRoot)        
        isNode = isinstance(node, GameNode)  
        self.assertEqual(isNode, True)
        
        
    def test_game_node_create_function(self):
        node = GameNode.create("asset/simple_3d_scene.xml")
        isNode = isinstance(node, GameNode)        
        isNodePath = isinstance(node.toNodePath(), NodePath)
        self.assertEqual(isNode, True)
        self.assertEqual(isNodePath , True)
        
    def test_active_scene(self):
        io = XmlIO("asset/simple_3d_scene.xml")
        xmlRoot = io.getRoot()
        builder = GameNodeBuilder()
        nodePath1 = builder.make(xmlRoot)
        
        io = XmlIO("asset/simple_3d_scene_2.xml")
        xmlRoot = io.getRoot()
        builder = GameNodeBuilder()
        nodePath2 = builder.make(xmlRoot)
        result = scenes.active(nodePath1)
        self.assertEqual(result , True)
        self.assertEqual(scenes.current, nodePath1)
        self.assertEqual(len(scenes.scenes), 1)
        
        result = scenes.active(nodePath2)
        self.assertEqual(result , True)
        self.assertEqual(scenes.current, nodePath2)
        self.assertEqual(len(scenes.scenes), 2)
        
    def test_swith_scene_async(self):
        print "TC: test_swith_scene_async"
        
        
    def test_single_async_scene(self):
        print "TC: test_single_async_scene"
        scene = GameNode.create("asset/simple_3d_scene.xml", True)
        
        
        
    
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    