'''
Created on Apr 18, 2014

@author: thinhtran
'''
import os
import unittest
from Monster.utility.io import XmlIO
from lxml import etree

class TestXMLLoaderFunction(unittest.TestCase):
    def setUp(self):
        # Load panda3d platform
        from panda3d.core import loadPrcFileData 
        loadPrcFileData('', 'window-type none')        
        import direct.directbase.DirectStart
    
    def test_convert_XML_to_OptionDefs(self):        
        io = XmlIO("asset/simple_xml_for_parsing_optiondefs.xml")
        optionDefs = io.getRoot()        
        self.assertEqual(optionDefs.tag, "gameobject")
        
        setPositionTag = etree.SubElement(optionDefs, "setPosition")
        self.assertEqual(setPositionTag.tag, "setPosition")
        
        objectType = optionDefs.get("type")
        self.assertEqual(objectType, "LightNode")
        
        
    def test_simple_game_node_queue(self):        
        io = XmlIO("asset/simple_game_node.xml")
        optionDefs = io.getRoot()
        cls = optionDefs.get("class")
        attrType= optionDefs.get("type")
        self.assertEqual(cls, "GameNode")
        self.assertEqual(attrType, "node")
        
    def test_simple_game_node_builder(self):
        io = XmlIO("asset/simple_game_node.xml")
        xmlRoot = io.getRoot()        
        from Monster.core.objects.gamenode import GameNodeBuilder
        builder = GameNodeBuilder()
        nodePath = builder.make(xmlRoot)
        self.assertEqual(nodePath.getX(), 2)
        self.assertEqual(nodePath.getName(), "simple_node")
        
    def test_count_node(self):
        io = XmlIO("asset/simple_game_node.xml")
        xmlRoot = io.getRoot()
        nNode = io.countNode()
        self.assertEqual(nNode, 2)
        
    def test_count_node2(self):        
        io = XmlIO("asset/simple_game_node.xml")
        nNode = io.countNode()
        self.assertEqual(nNode, 2)
        
        
    def test_recursive_game_node_builder(self):
        io = XmlIO("asset/simple_game_node.xml")
        xmlRoot = io.getRoot()
        from Monster.core.objects.gamenode import GameNodeBuilder
        builder = GameNodeBuilder()
        nodePath = builder.make(xmlRoot)
        self.assertEqual(nodePath.find("child_node").getX(), 7.5)
        self.assertEqual(nodePath.find("child_node").getName(), "child_node")
        
        
        
        
        
         
        
    
    

if __name__ == "__main__":    
    unittest.main()
