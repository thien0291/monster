""" I cannot test background loading without running Panda3d app. If we need to test this case, we have to uncomment
lines of code and run this file.
"""

#from panda3d.core import loadPrcFileData, NodePath
#
#loadPrcFileData('', 'notify-level-Monster debug')
##loadPrcFileData('', 'notify-level-Monster.core.objects.gamenode.GameNode debug')
#loadPrcFileData('', 'notify-level-Monster.core.modules.managers.scenemanager.SceneManager debug')
#
#import direct.directbase.DirectStart
#from Monster.core.objects.gamenode import GameNode
#
##myscene = GameNode.create("asset/bgloading_loading_scene.xml", True)
##myscene.toNodePath().reparentTo(render)
#scenes.activeWithLoadScene("asset/bgloading_loading_scene.xml", "asset/bgloading_target_scene.xml")
#
#
## expected print lines:
##:Monster.core.modules.managers.scenemanager.SceneManager(debug): SceneManager.active: load_scene
##:Monster.core.modules.managers.scenemanager.SceneManager(debug): SceneManager._asyncLoadedPart: mystery_box
##:Monster.core.modules.managers.scenemanager.SceneManager(debug): SceneManager._asyncLoadedPart: smiley_ball
##:Monster.core.modules.managers.scenemanager.SceneManager(debug): SceneManager.active: play_scene
#
#run()