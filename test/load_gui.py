'''
Created on Apr 18, 2014

@author: thinhtran
'''
import unittest
from direct.gui.DirectGui import *

from panda3d.core import loadPrcFileData, NodePath 
from Monster.core.objects.gamenode import GameNode
from Monster.extend.objects.directgui import *


loadPrcFileData('', 'window-type none')
loadPrcFileData('', 'notify-level-Monster debug')

class TestGui(unittest.TestCase):
    def setUp(self):
        import direct.directbase.DirectStart
        from Monster.extend.objects.directgui import *
        builder = DirectGUIBuilder()
        
        
        
    
    def test_guilayer_inherit_from_list(self):        
        guilayer = DirectGUILayer("mygui")
        
        b = DirectButton(text = ("OK", "click!", "rolling over", "disabled"))
        guilayer.append(b)
        self.assertEqual(guilayer[0], b)
        
        
    def test_load_gui_from_xml(self):
        import sys
        from Monster.extend.objects.directgui import *
        print "system modules: ", getattr(sys.modules[__name__],"DirectGUIBuilder")
        
        rootNode = GameNode.create("asset/simple_gui_layer_in_3d_scene.xml")
        
        
        
        
        
        
    
    
    
    

if __name__ == "__main__":    
    unittest.main()
    
  