'''
Created on Apr 18, 2014

@author: thinhtran
'''
import unittest
import os
from Monster.core.objects.gamenode import GameNode

class TestGameNode(unittest.TestCase):

    def setUp(self):
        # Load panda3d platform
        from panda3d.core import loadPrcFileData 
        loadPrcFileData('', 'window-type none')        
        import direct.directbase.DirectStart
        testFolder = os.path.join(os.getcwd(), "test")
        if os.path.exists(testFolder):        
            os.chdir(testFolder)

    def test_initialize_gamenode(self):        
        mynode = GameNode("hehe")
        optionDefs = [
            [[2, 3, 4], 'setPos', None]
        ]                
        mynode.refresh(optionDefs)
        
        
    def test_apply_recursive_method(self):
        mynode = GameNode.create("asset/simple_game_node.xml")           
        mynode.applyRecursive("setPos", [3, 4, 5])        
        nodePath = mynode.toNodePath()        
        pos = nodePath.getPos()
        child = nodePath.find("child_node")
        childPos = child.getPos()
        self.assertEqual(pos.x, 3)
        self.assertEqual(childPos.y, 4)
        
        
        
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()